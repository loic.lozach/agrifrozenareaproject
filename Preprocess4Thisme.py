#!/usr/bin/python
__author__ = "Loic Lozach"
__date__ = "$Dec 14, 2018 12:40:21 PM$"


import json, os, sys, argparse, zipfile
from subprocess import Popen, PIPE
try:
    from osgeo import ogr, osr, gdal
except:
    sys.exit('ERROR: cannot find GDAL/OGR modules')
import otbApplication


def process_command(cmd,chgdir):
    print("Starting : "+" ".join(cmd))
    print("working dir : "+chgdir)
    wd = os.getcwd()
    os.chdir(chgdir)
    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    
    os.chdir(wd)
    return p.returncode


def create_dict_assets(smhref, qklhref):
    mvassets = {
            "shapefile":{
                "href":smhref,
                "type":"application/zip"
            }, "thumbnail":{
                "href":qklhref,
                "type":"image/png",
                "roles":["thumbnail"]
            }
    }
    return mvassets

def create_dict_properties(datetime,platform,gsd,epsg,title,description):
    #gsd => resolution en metre
    mvproperties = {
        "datetime":datetime,
        "title":title,
        "description":description,
        "platform":platform,
        "eo:gsd":gsd,
        "eo:epsg":epsg
    }
    return mvproperties

def create_dict_geometry(coordinates,geomtype="Polygon",epsg=2154):
    mvgeom = {
        "type":geomtype,
        "coordinates":[coordinates],
        "crs":{
            "type":"name",
             "properties":{
                  "name":"epsg:"+str(epsg)
             }
        }
    }
    return mvgeom


def create_dict_image(imgid, geometry, properties, assets, imgtype="Feature"):
    mvdata = {
        "id":imgid,
        "type":imgtype,
        "geometry":geometry,
        "properties":properties,
        "assets":assets
    }
    
    return mvdata

def search_files(directory='.', resolution='S1', extension='SAFE', fictype='d'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit()
            
    return images


def generate_quicklook(shp, ref, lut, quick):
    
    app0 = otbApplication.Registry.CreateApplication("Rasterization")
    app0.SetParameterString("in",shp)
    app0.SetParameterString("out", "temp0.tif")
    app0.SetParameterString("im", ref)
    app0.SetParameterString("mode", "attribute")
    app0.SetParameterString("mode.attribute.field", "FROZ_TYPE")
    app0.SetParameterInt("background", 255)
    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    app0.Execute()
    # The following line creates an instance of the Superimpose application
    app1 = otbApplication.Registry.CreateApplication("ColorMapping")
    # The following lines set all the application parameters:
    app1.SetParameterInputImage("in", app0.GetParameterOutputImage("out"))
    app1.SetParameterString("out", "temp1.tif")
    app1.SetParameterString("op", "labeltocolor")
    app1.SetParameterString("method", "custom")
    app1.SetParameterString("method.custom.lut", lut)
    
    print("Launching... Resampling")
    # The following line execute the application
    app1.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
    app2 = otbApplication.Registry.CreateApplication("Quicklook")
    
    # The following lines set all the application parameters:
    app2.SetParameterInputImage("in", app1.GetParameterOutputImage("out"))
    app2.SetParameterString("out", quick)
    app2.SetParameterInt("sx", 1024)
    
    print("Launching... Resampling")
    # The following line execute the application
    app2.ExecuteAndWriteOutput() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
def reprojectRaster(absfile):
    temp_file = absfile + ".old"
    os.rename(absfile, temp_file)
    
    
    p = Popen(['gdalwarp', '-t_srs', 'EPSG:4326', temp_file, absfile], stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("gdalwarp failed %d : %s" % (p.returncode, output))
        with open("gdalwrap_log.err",'a') as err:
            err.write("######################################################################################################")
            err.write(absfile)
            err.write(output)
            err.write("######################################################################################################")
            os.remove(absfile)
            os.rename(temp_file, absfile)
        return 1
        
    print("gdalwarp succeeded on : "+absfile)
    os.remove(temp_file)
    return 0

def get_coordinates(img):
    raster = gdal.Open(img)
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    
    mv_epsg = int(proj.GetAttrValue('AUTHORITY',1))
    
    upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
    cols = raster.RasterXSize
    rows = raster.RasterYSize
     
    ulx = upx + 0*xres + 0*xskew
    uly = upy + 0*yskew + 0*yres
     
    llx = upx + 0*xres + rows*xskew
    lly = upy + 0*yskew + rows*yres
     
    lrx = upx + cols*xres + rows*xskew
    lry = upy + cols*yskew + rows*yres
     
    urx = upx + cols*xres + 0*xskew
    ury = upy + cols*yskew + 0*yres
    
    pointLL, pointUL, pointUR, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointLL.AddPoint(llx, lly)
    pointUR.AddPoint(urx, ury)
    pointUL.AddPoint(ulx, uly)
    pointLR.AddPoint(lrx, lry)
    
#     if not proj.IsGeographic() :
#         
#         outSpatialRef = osr.SpatialReference()
#         outSpatialRef.ImportFromEPSG(4326)
#         coordTransform = osr.CoordinateTransformation(proj, outSpatialRef)
#         
#         pointLL.Transform(coordTransform)
#         pointUR.Transform(coordTransform)
#         pointUL.Transform(coordTransform)
#         pointLR.Transform(coordTransform)
    
    return mv_epsg, xres, [[pointLL.GetX(),pointLL.GetY()],[pointUL.GetX(),pointUL.GetY()],[pointUR.GetX(),pointUR.GetY()],[pointLR.GetX(),pointLR.GetY()],[pointLL.GetX(),pointLL.GetY()]]
    
if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Create zipfile, geojson metadata and png quicklook from freeze detection shapefile for THISME ingestion 
        Output directory must be in /mnt/GEOSUD_WA/SoilMoisture/FreezeDetection
        """)
    

    #####################################################
    driver = gdal.GetDriverByName('GTiff')
    driver.Register()
    #####################################################    
    parser.add_argument('-indir', action='store', required=True, help="Directory to find recursively shapefiles")
    parser.add_argument('-outdir', action='store', required=True, help="Output directory for zipfiles")
    parser.add_argument('-refimg', action='store', required=True, help="Reference raster for rasterization, RPG tif file")
    parser.add_argument('-lut', action='store', required=False, help='LUT file for quicklook generation')
    parser.add_argument('-overwrite', choices=['no', 'all',"zip","json","png"],  default='no', required=False, help='[Optional] Overwrite already existing files, default no')

    
    args=parser.parse_args()
    if not os.path.exists(args.indir):
        print("Error: Directory does not exist "+args.indir)
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
        
        
    shpfiles = search_files(args.indir,resolution='FREEZEDETECT_', extension='shp', fictype='f')
    shpexts = ["shp","shx","dbf","prj"]
    
    mv_epsg, pixelWidth, mv_coordinates = get_coordinates(args.refimg)
    
    for shp in shpfiles :
        dirshp = os.path.dirname(shp)
        outzip = os.path.basename(shp)[:-3]+"zip"
        outabszip = os.path.join(args.outdir,outzip)
        ashp = outzip[:-3]+"*"
        geojsonfile = outabszip[:-3]+"JSON"
        quicklookfile = outabszip[:-3]+"PNG"
        
        print("################################################################")
        print("Processing file " + outabszip)
        
        if not os.path.exists(outabszip) or args.overwrite == "all" or args.overwrite == "zip":
#             cmd=["zip", os.path.join(args.outdir,outzip) , ashp ]
#             process_command(cmd, dirshp) 
            with zipfile.ZipFile(os.path.join(args.outdir,outzip), "w", compression=zipfile.ZIP_DEFLATED) as zf:
                for shpex in shpexts :
                    tozip = shp[:-3]+shpex
                    zf.write(tozip, os.path.basename(tozip))
        else:
            print("Already exists. Passing. ")


        print("Processing file " + geojsonfile)
        if not os.path.exists(geojsonfile) or args.overwrite == "all" or args.overwrite == "json" :
            spltpath = outabszip.split("/")
            spltfilename = outzip[:-4].split("_")

            ind = spltpath.index("SoilMoisture")
            mv_url = "https://products.thisme.cines.teledetection.fr/" + '/'.join(spltpath[ind+1:])
            qkl_url = mv_url[:-3]+"PNG"
            
            date = spltfilename[2]
            date_in_format = date[0:4]+"-"+date[4:6]+"-"+date[6:8]+date[8:11]+":"+date[11:13]+":"+date[13:15]+"Z"

            mv_title=spltfilename[1]
            mv_description="Detection of frozen soils in agricultural areas over MRGS tile #"+mv_title
            if "DES" in shp:
                orb = "DES"
            else:
                orb = "ASC"
            mv_plateform = "S1X-"+orb
            
            
            mv_properties = create_dict_properties(date_in_format,mv_plateform,pixelWidth,mv_epsg,mv_title,mv_description)
            
            mv_assets = create_dict_assets(mv_url, qkl_url)
            
            mv_geometry = create_dict_geometry(mv_coordinates)
            
            mv_geojson = create_dict_image(outzip[:-4], mv_geometry, mv_properties, mv_assets)
            
            print("Writing json file.")
            with open(geojsonfile,'w') as jsonf:
                json.dump(mv_geojson, jsonf, indent=4)
        else:
            print("Already exists. Passing. ")
        
        print("Processing file " + quicklookfile)
        if not os.path.exists(quicklookfile) or args.overwrite == "all" or args.overwrite == "png" :
            print("Generating quicklook")
            generate_quicklook(shp, args.refimg, args.lut, quicklookfile)
        else:
            print("Already exists. Passing. ")
        
    
    print("################################################################")
    print("Process done.")
