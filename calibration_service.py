#!/usr/bin/python3
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import os, shutil, glob, datetime
import xml.etree.ElementTree as ET
import argparse
import otbApplication, math
from osgeo import gdal, osr, ogr
from subprocess import Popen, PIPE
from configparser import ConfigParser

def read_config(path:str=None):
    config = ConfigParser()
    config.read(path)

    sections_dict = {}

    # get all defaults
    defaults = config.defaults()
    temp_dict = {}
    for key in defaults.keys():
        temp_dict[key] = defaults[key]

    sections_dict['default'] = temp_dict

    # get sections and iterate over each
    sections = config.sections()
    
    for section in sections:
        options = config.options(section)
        temp_dict = {}
        for option in options:
            temp_dict[option] = config.get(section,option)
        
        sections_dict[section] = temp_dict

    return sections_dict

globconfig = {
    "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
    "srtmhgtzip":"/data/SRTM/SRTMHGTZIP",
    "demtif":"/data/SRTM/SRTMHGTZIP/TIF",
    "geoid":"/work/python/data/egm96.grd"
    }
if os.path.exists("/work/python/config.ini"):
    globconfig = read_config("/work/python/config.ini")["DEMDATA"]
    if not os.path.exists(globconfig["geoid"]):
        print("Error: Can't find geoid file at: "+globconfig["geoid"])
        print("Geoid can be download here: https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb-data/blob/master/Input/DEM/egm96.grd")
        exit()
    if not os.path.exists(globconfig["srtmhgtzip"]):
        print("Can't find SRTM folder. Creating it at :"+globconfig["srtmhgtzip"])
        os.makedirs(globconfig["srtmhgtzip"])
else:
    print("Error: Can't find /work/python/config.ini file")
    exit()

namespaces = {
    'gml': "http://www.opengis.net/gml"
    }
overwrite=False

def search_files(directory='.', resolution='S1', extension='SAFE', fictype='d'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit()
            
    return images

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode

def get_img_extend(img):
    raster = gdal.Open(img)
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    
    upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
    cols = raster.RasterXSize
    rows = raster.RasterYSize
     
    ulx = upx + 0*xres + 0*xskew
    uly = upy + 0*yskew + 0*yres
     
    llx = upx + 0*xres + rows*xskew
    lly = upy + 0*yskew + rows*yres
     
    lrx = upx + cols*xres + rows*xskew
    lry = upy + cols*yskew + rows*yres
     
    urx = upx + cols*xres + 0*xskew
    ury = upy + cols*yskew + 0*yres
    
    pointLL, pointUL, pointUR, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointLL.AddPoint(llx, lly)
    pointUR.AddPoint(urx, ury)
    pointUL.AddPoint(ulx, uly)
    pointLR.AddPoint(lrx, lry)
    
    if not proj.IsGeographic() :
        
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(4326)
        coordTransform = osr.CoordinateTransformation(proj, outSpatialRef)
        
        pointLL.Transform(coordTransform)
        pointUR.Transform(coordTransform)
        pointUL.Transform(coordTransform)
        pointLR.Transform(coordTransform)
        
    return pointLL, pointUL, pointUR, pointLR, cols, rows

def get_gcp_extend(img):
    raster = gdal.Open(img)
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    rgcp = raster.GetGCPs()
    print([rgcp[0].GCPX,rgcp[0].GCPY])
    print([rgcp[-1].GCPX,rgcp[-1].GCPY])
    
    pointLL, pointUL, pointUR, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    for r in rgcp:
        if r.GCPPixel == 0 and r.GCPLine == 0:
            pointLL.AddPoint(r.GCPX, r.GCPY)
        elif r.GCPPixel == 0 and r.GCPLine == rgcp[-1].GCPLine:
            pointUL.AddPoint(r.GCPX, r.GCPY)
        elif r.GCPPixel == rgcp[-1].GCPPixel and r.GCPLine == 0:
            pointLR.AddPoint(r.GCPX, r.GCPY)
        elif r.GCPPixel == rgcp[-1].GCPPixel and r.GCPLine == rgcp[-1].GCPLine:
            pointUR.AddPoint(r.GCPX, r.GCPY)

    
        
    return pointLL, pointUL, pointUR, pointLR


def get_img_intersection(img,ref):
    
    pointLL, pointUL, pointUR, pointLR = get_gcp_extend(img)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(pointLL.GetX(), pointLL.GetY())
    ring.AddPoint(pointUL.GetX(), pointUL.GetY())
    ring.AddPoint(pointUR.GetX(), pointUR.GetY())
    ring.AddPoint(pointLR.GetX(), pointLR.GetY())
    ring.AddPoint(pointLL.GetX(), pointLL.GetY())
    
    # Create polygon
    imgpoly = ogr.Geometry(ogr.wkbPolygon)
    imgpoly.AddGeometry(ring)
    
    
    pointLL, pointUL, pointUR, pointLR, cols, rows = get_img_extend(ref)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(pointLL.GetX(), pointLL.GetY())
    ring.AddPoint(pointUL.GetX(), pointUL.GetY())
    ring.AddPoint(pointUR.GetX(), pointUR.GetY())
    ring.AddPoint(pointLR.GetX(), pointLR.GetY())
    ring.AddPoint(pointLL.GetX(), pointLL.GetY())
    
    # Create polygon
    refpoly = ogr.Geometry(ogr.wkbPolygon)
    refpoly.AddGeometry(ring)
    
    if refpoly.Within(imgpoly):
        return refpoly
    
    return refpoly.Intersection(imgpoly)
    
    
    
def find_srtm_hgt_name(llx, lly , urx, ury ):
    
    
    pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointLL.AddPoint(llx, lly)
    pointUR.AddPoint(urx, ury)
    
    tilesname=[]
    print("LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
    
    for o in range(math.ceil(pointUR.GetX()) - math.floor(pointLL.GetX())):
        for a in range(math.ceil(pointUR.GetY()) - math.floor(pointLL.GetY())):
            lat =  math.floor(pointLL.GetY()) + a
            lon =  math.floor(pointLL.GetX()) + o
            if lat >= 0 :
                hem = 'N'
            else : 
                hem = 'S'
                lat = abs(lat)
            if lon >= 0 :
                grw = 'E'
            else : 
                grw = 'W'
                lon = abs(lon)
            tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
            
    return tilesname

def get_srtm_tiles_from_S1(sarsafe):
    #get tiles
    tree = ET.parse(os.path.join(sarsafe,"manifest.safe"))
    root = tree.getroot()
    
    footprintnode = root.find(".//gml:coordinates",namespaces)
    footprintcoordsp = footprintnode.text.split(" ")
    footprintlats = []
    footprintlons = []
    for coord in footprintcoordsp:
        coordsp = coord.split(",")
        footprintlats.append(float(coordsp[0]))
        footprintlons.append(float(coordsp[1]))
        
    llx, lly = min(footprintlons),min(footprintlats)
    urx, ury = max(footprintlons),max(footprintlats)
    
    tilesname = find_srtm_hgt_name(llx, lly , urx, ury )
    return tilesname


def prepare_srtm_hgt(tilesname):
    
    
    print("Checking SRTM tiles: "+str(tilesname))
    tilefiles = []
    if len(tilesname) == 0:
        print("bug")
        exit()
    
    srtmsuf = ".SRTMGL1.hgt.zip"   
    
    tiftiles=[]
    for tile in tilesname :
        tilef = os.path.join(globconfig["srtmhgtzip"],tile + srtmsuf)
        if not os.path.exists(tilef):
            print("Download SRTM tile : "+tilef)
            cmd=["wget","-P", globconfig['srtmhgtzip'],globconfig["urlsrmt"]+tile + srtmsuf]
            rcode = process_command(cmd)
            
            if rcode != 0:
                continue
        
        hgtdir = os.path.join(globconfig["srtmhgtzip"],"HGT")
        tifdir = os.path.join(globconfig["srtmhgtzip"],"TIF")
        if not os.path.exists(hgtdir):
            os.mkdir(hgtdir)
        if not os.path.exists(tifdir):
            os.mkdir(tifdir)
            
        tilehgt = os.path.join(hgtdir,tile+".hgt")
        tiletif = os.path.join(tifdir,tile+".tif")
        if not os.path.exists(tilehgt):
            print("Unzip SRTM tile : "+tilehgt)
            cmd=['unzip','-d',hgtdir,tilef]   
            process_command(cmd)
        
        if not os.path.exists(tiletif):
            print("Convert SRTM tile : "+tiletif)
            cmd=['gdal_translate',tilehgt,tiletif]   
            process_command(cmd)
        
        tiftiles.append(tiletif)
    
    print("Ok for SRTM tiles: "+str(tilesname))
    return tiftiles

def create_zone_dem_aspect_and_slope(ref,zone,outdir):
    
    outdem = os.path.join(outdir,"SRTM_"+zone+".TIF")
    if os.path.exists(outdem) and not overwrite :
        return outdem
        
    #get tiles
    pointLL, pointUL, pointUR, pointLR, cols, rows = get_img_extend(ref)
    
    tilesname = find_srtm_hgt_name(pointLL.GetX()-1.5, pointLL.GetY()-1 , pointUR.GetX()+1.5, pointUR.GetY()+1 )
    tilefiles = prepare_srtm_hgt(tilesname)
    
    if len(tilefiles) == 0 :
        print("Error : Can't find tiles")
        exit()
    
    print("Generating DEM file on ref:"+ref)
    
    mosapp = otbApplication.Registry.CreateApplication("Mosaic")
    mosparams = {"il":tilefiles, "out":"mosatemp.tif"}
    mosapp.SetParameters(mosparams)
    mosapp.Execute()
    
    supapp = otbApplication.Registry.CreateApplication("Superimpose")
    supapp.SetParameterString("inr", ref)
    supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
    supapp.SetParameterString("out", outdem)
    supapp.ExecuteAndWriteOutput()
    
    #Compute slope & aspect
    print("Generating slope file...")
    outslope = os.path.join(outdir,"SLOPE_"+zone+".TIF")
    cmd=['gdaldem', 'slope', '-p', outdem, outslope]
    
    process_command(cmd)
    
    print("Generating aspect file...")
    outaspect = os.path.join(outdir,"ASPECT_"+zone+".TIF")
    cmd=['gdaldem', 'aspect', outdem, outaspect]
    
    process_command(cmd)
    
    return outdem
        
def create_incidenceAngle_from_surface_raster(annofiles, img, outinc):

    vrtfile = outinc.replace("_THETASURF.TIF","_THETA.vrt")
    csvfile = outinc.replace("_THETASURF.TIF","_THETA.csv")
    layername = os.path.basename(vrtfile)[:-4]
    
    
    pointLL, pointUL, pointUR, pointLR, cols, rows = get_img_extend(img)
    
    if not os.path.exists(csvfile):
        for annofile in annofiles:
    
            tree = ET.parse(annofile)
            root = tree.getroot()
            
            geoloclist = root.find(".//geolocationGridPointList")
            
            if geoloclist == None :
                print("Can't find geolocationGridPointList tag in xml file.")
                exit()
            
            header = True
            if os.path.exists(csvfile):
                header = False
            if not os.path.exists(vrtfile):
                with open(vrtfile,'w') as vrt:
                    vrt.write("<OGRVRTDataSource>\n")
                    vrt.write('    <OGRVRTLayer name="'+layername+'">\n')
                    vrt.write('        <SrcDataSource>'+csvfile+'</SrcDataSource>\n')
                    vrt.write("        <GeometryType>wkbPoint</GeometryType>\n")
                    vrt.write("        <LayerSRS>WGS84</LayerSRS>\n")
                    vrt.write('        <GeometryField encoding="PointFromColumns" x="Easting" y="Northing" z="incidence"/>\n')
                    vrt.write("    </OGRVRTLayer>\n")
                    vrt.write("</OGRVRTDataSource>\n")
                
            with open(csvfile,'a') as csvf:
                if header:
                    csvf.write("Easting,Northing,incidence\n")
                for child in geoloclist:
                    lat = child.find(".//latitude").text
                    lon = child.find(".//longitude").text
                    alti = float(child.find(".//height").text)
                    incdem = float(child.find(".//incidenceAngle").text)
                    inc = incdem - alti/693000.
                    csvf.write(lon+","+lat+","+str(incdem)+"\n")
     
    
    cmd = ["gdal_grid","-a","linear","-txe", str(pointLL.GetX()), str(pointUR.GetX()), "-tye", str(pointLL.GetY()), str(pointUR.GetY()), "-outsize", str(cols), str(rows), "-of", "GTiff", "-ot", "Float32", "-l", layername, vrtfile, outinc]
    
    process_command(cmd)
    
    
    return (outinc, vrtfile, csvfile)

def create_incidenceAngle_from_ellipsoid_raster(vvortho,dem,thetasurf,outthetaelli):
    
    
    app41 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app41.SetParameterString("inm", thetasurf)
    app41.SetParameterString("inr", vvortho)
    app41.SetParameterString("interpolator","bco")
    app41.SetParameterString("out", "temp41.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app41.Execute()
    print("End of Resampling \n")
    
    app42 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app42.SetParameterString("inm", dem)
    app42.SetParameterString("inr", vvortho)
    app42.SetParameterString("interpolator","bco")
    app42.SetParameterString("out", "temp41.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app42.Execute()
    print("End of Resampling \n")
    
    appS = otbApplication.Registry.CreateApplication("BandMath")
    appS.SetParameterStringList("il", [vvortho])
    appS.AddImageToParameterInputImageList("il", app42.GetParameterOutputImage("out"))
    appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out"))
    # Define Input im2: Band Red (B4)
    appS.SetParameterString("out", "tempS.tif")
    appS.SetParameterString("exp", "im1b1 == 0?0:im3b1 - im2b1/693000" )
    
    appS.Execute()
    
    appM = otbApplication.Registry.CreateApplication("ManageNoData")
    appM.SetParameterInputImage("in", appS.GetParameterOutputImage("out"))
    appM.SetParameterString("out", outthetaelli+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    appM.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    appM.SetParameterString("mode", "changevalue")
    appM.ExecuteAndWriteOutput()
    
    return outthetaelli

def create_local_incidenceAngle_raster(annofiles, img, dem, thetasurf, outthetaloc):

    inslope = dem.replace("SRTM_", "SLOPE_")
    inaspect = dem.replace("SRTM_", "ASPECT_")

    plateformheading=[]
    for annofile in annofiles:
    
        tree = ET.parse(annofile)
        root = tree.getroot()
        
        plateformheading.append(float(root.find(".//platformHeading").text))
        
    if len(plateformheading) == 0:
        print("Error: Can't find plateformHeading in annotation files")
        exit()
    elif len(plateformheading) == 1:
        azimuthsat = (plateformheading[0]+90) % 360
    else:
        azimuthsat = (math.fsum(plateformheading)/len(plateformheading)+90) % 360
        

    
    app41 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app41.SetParameterString("inm", thetasurf)
    app41.SetParameterString("inr", img)
    app41.SetParameterString("interpolator","bco")
    app41.SetParameterString("out", "temp41.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app41.Execute()
    print("End of Resampling \n")
    
    app43 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app43.SetParameterString("inm", inslope)
    app43.SetParameterString("inr", img)
    app43.SetParameterString("interpolator","bco")
    app43.SetParameterString("out", "temp43.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app43.Execute()
    print("End of Resampling \n")
    
    app44 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app44.SetParameterString("inm", inaspect)
    app44.SetParameterString("inr", img)
    app44.SetParameterString("interpolator","bco")
    app44.SetParameterString("out", "temp44.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app44.Execute()
    print("End of Resampling \n")
    
    appS = otbApplication.Registry.CreateApplication("BandMath")
    appS.SetParameterStringList("il", [img])  #im1b1 -> vv
    appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out")) #im2b1 -> incidence surf
    appS.AddImageToParameterInputImageList("il", app43.GetParameterOutputImage("out")) #im3b1 -> slope
    appS.AddImageToParameterInputImageList("il", app44.GetParameterOutputImage("out")) #im4b1 -> aspect
    # Define Input im2: Band Red (B4)
    appS.SetParameterString("out", "tempS.tif")
    appS.SetParameterString("exp", "im1b1 == 0?0:acos(cos(im3b1*"+str(math.pi/400)+")*cos(im2b1*"+str(math.pi/180)+")-sin(im3b1*"+str(math.pi/400)+")*sin(im2b1*"+str(math.pi/180)+")*cos(("+str(azimuthsat)+"-im4b1)*"+str(math.pi/180)+"))*"+str(180/math.pi)+"")
    
    appS.Execute()
    
    appM = otbApplication.Registry.CreateApplication("ManageNoData")
    appM.SetParameterInputImage("in", appS.GetParameterOutputImage("out"))
    appM.SetParameterString("out", outthetaloc+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    appM.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    appM.SetParameterString("mode", "changevalue")
    appM.ExecuteAndWriteOutput()
    
    return outthetaloc

def create_tmp_shp(name, poly, outdir):

    shpfile = name+".shp"
    outshp = os.path.join(outdir,shpfile)
    
    driver = ogr.GetDriverByName("ESRI Shapefile")
        
    # create the data source
    data_source = driver.CreateDataSource(outshp)
    
    # create the spatial reference, WGS84
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    
    # create the layer
    layer = data_source.CreateLayer(name, srs, ogr.wkbMultiPolygon)
     
    # create the feature
    feature = ogr.Feature(layer.GetLayerDefn())
    
    feature.SetGeometry(poly)
    # Create the feature in the layer (shapefile)
    layer.CreateFeature(feature)
    
    return outshp

def otb_s1_pipeline(sarfiles, ref, outfile):
    
    print("processing.......................................................")

    outdir = os.path.dirname(outfile)
    appdict={"ExtractShp":[],"ExtractROI":[],"SARCalibration":[],"OrthoRectification":[],
             "Mosaic":None,"BandMath":None,"ManageNoData":None
             }
    
    d=0
    delsar = []
    for sar in sarfiles:
        interpoly = get_img_intersection(sar,ref)
        print(interpoly)
        if None == interpoly or interpoly.IsEmpty():
            print("Not using "+sar)
            delsar.append(d)
        d+=1
    for dd in delsar:
        sarfiles.pop(dd)
        
    if len(sarfiles) == 0:
        print("ERROR: Sar files don't intersect reference.")
        print("Passing....")
        return 1
    i=0
    for sar in sarfiles:
        
        interpoly = get_img_intersection(sar,ref)
#         shpname = os.path.basename(sar)[:-5]
        appdict["ExtractShp"].append(create_tmp_shp("tmppoly"+str(i), interpoly, outdir)) #shpname
        
        appdict["SARCalibration"].append(otbApplication.Registry.CreateApplication("SARCalibration"))
        appdict["SARCalibration"][i].SetParameterString("in",sar)
        appdict["SARCalibration"][i].SetParameterString("out",str(i)+"SARCalibration.tif") 
        appdict["SARCalibration"][i].Execute()
        
        appdict["OrthoRectification"].append(otbApplication.Registry.CreateApplication("OrthoRectification"))
        appdict["OrthoRectification"][i].SetParameterInputImage("io.in",appdict["SARCalibration"][i].GetParameterOutputImage("out"))
        appdict["OrthoRectification"][i].SetParameterString("io.out",str(i)+"OrthoRectification.tif")
        appdict["OrthoRectification"][i].SetParameterFloat("opt.gridspacing",40)
        appdict["OrthoRectification"][i].SetParameterString("elev.dem", globconfig["demtif"])
        appdict["OrthoRectification"][i].SetParameterString("elev.geoid", globconfig["geoid"])
        appdict["OrthoRectification"][i].Execute()  
              
        appdict["ExtractROI"].append(otbApplication.Registry.CreateApplication("ExtractROI"))
        appdict["ExtractROI"][i].SetParameterInputImage("in",appdict["OrthoRectification"][i].GetParameterOutputImage("io.out"))
        appdict["ExtractROI"][i].SetParameterString("out",str(i)+"ExtractROI.tif") 
        appdict["ExtractROI"][i].SetParameterString("mode","fit") 
        appdict["ExtractROI"][i].SetParameterString("mode.fit.vect",appdict["ExtractShp"][i]) 
        appdict["ExtractROI"][i].Execute()
        
        i+=1
        
    if len(sarfiles) > 1:
            
        appdict["Mosaic"] = otbApplication.Registry.CreateApplication("Mosaic")
        for j in range(len(sarfiles)):
            appdict["Mosaic"].AddImageToParameterInputImageList("il", appdict["ExtractROI"][j].GetParameterOutputImage("out"))
        appdict["Mosaic"].SetParameterString("out",  "mosatemp.tif")
        appdict["Mosaic"].Execute()
        
        appdict["BandMath"] = otbApplication.Registry.CreateApplication("BandMath")
        appdict["BandMath"].AddImageToParameterInputImageList("il", appdict["Mosaic"].GetParameterOutputImage("out"))
        appdict["BandMath"].SetParameterString("out", "BMtemp.tif")
        appdict["BandMath"].SetParameterString("exp", "im1b1<=0?0:im1b1")
        appdict["BandMath"].Execute()
        
    else : 
        
        appdict["BandMath"] = otbApplication.Registry.CreateApplication("BandMath")
        appdict["BandMath"].AddImageToParameterInputImageList("il", appdict["ExtractROI"][0].GetParameterOutputImage("out"))
        appdict["BandMath"].SetParameterString("out", "BMtemp.tif")
        appdict["BandMath"].SetParameterString("exp", "im1b1<=0?0:im1b1")
        appdict["BandMath"].Execute()
        
    
    appdict["ManageNoData"] = otbApplication.Registry.CreateApplication("ManageNoData")
    appdict["ManageNoData"].SetParameterInputImage("in", appdict["BandMath"].GetParameterOutputImage("out"))
    appdict["ManageNoData"].SetParameterString("out", outfile+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    appdict["ManageNoData"].SetParameterString("mode", "changevalue")
    appdict["ManageNoData"].ExecuteAndWriteOutput()
    
    
    shapefiles = glob.glob(appdict["ExtractShp"][0][:-5]+"*")
    for s in shapefiles:
        os.remove(s)
    
    print("done.............................................................")
    
    return 0
        
def sigma0_at_40d(sigma0, incidloc, out40):
    
    app1 = otbApplication.Registry.CreateApplication("BandMath")
    app1.SetParameterStringList("il",[sigma0,incidloc])
    app1.SetParameterString("out", "temp.tif")
    app1.SetParameterString("exp", "im1b1*(cos("+str(math.pi/180)+"*40)^2)/(cos("+str(math.pi/180)+"*im2b1)^2) > 0 ?"+
                            " im1b1*(cos("+str(math.pi/180)+"*40)^2)/(cos("+str(math.pi/180)+"*im2b1)^2): 0")
    app1.Execute()
    
    app2 = otbApplication.Registry.CreateApplication("ManageNoData")
    app2.SetParameterInputImage("in", app1.GetParameterOutputImage("out"))
    app2.SetParameterString("out", out40+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    app2.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    app2.SetParameterString("mode", "changevalue")
    app2.ExecuteAndWriteOutput()
    
def get_envelope_from_db(tilename, labelshpfile):
    
    ##TODO SQL a verifier
    cmd = ["ogr2ogr", "-f", "ESRI Shapefile", labelshpfile, "-nln", os.path.basename(labelshpfile)[:-4], "-nlt", "MULTIPOLYGON",
           "PG:dbname='postgres' host='db' port='5432' user='postgres' password='postgres'",
            "-sql", """SELECT pg.* FROM frozenapp_parcellegraphique pg, frozenapp_tileenvelope te, frozenapp_tileenvelope_parcelles pgte 
            WHERE pgte.parcellegraphique_id=pg.id AND pgte.tileenvelope_id=te.id AND te.tile="""+"'"+tilename+"'" 
            ]
    code = process_command(cmd)
    if code != 0 :
        exit()
        
    
def create_labelimage_ref(tilename, outdir):

    labelshpfile = os.path.join(outdir,"RPG_"+tilename+".shp")
    labeltifout = os.path.join(outdir,"RPG_"+tilename+".TIF")
    labelsfield="id_parcel"
    
    print("#Creating Reference"+"RPG_"+tilename+".shp")
    
    get_envelope_from_db(tilename, labelshpfile)  
    
    driver = ogr.GetDriverByName('ESRI Shapefile')
    
    dataSource = driver.Open(labelshpfile, 0)
    
    layer = dataSource.GetLayer()
    shpextent = layer.GetExtent() # [xmin,xmax,ymin,ymax]
    print(shpextent)
    szx = int((shpextent[1] - shpextent[0])/10)
    szy = int((shpextent[3] - shpextent[2])/10)

    print("#Creating Reference"+"RPG_"+tilename+".TIF")
    
    app1 = otbApplication.Registry.CreateApplication("Rasterization")
    app1.SetParameterString("in", labelshpfile)
    app1.SetParameterString("out", labeltifout)
    app1.SetParameterInt("szx", szx)
    app1.SetParameterInt("szy", szy)
    app1.SetParameterInt("epsg", 2154)
    app1.SetParameterFloat("orx", shpextent[0])
    app1.SetParameterFloat("ory", shpextent[3])
    app1.SetParameterFloat("spx", 10.)
    app1.SetParameterFloat("spy", -10.)
    app1.SetParameterString("mode", "attribute")
    app1.SetParameterString("mode.attribute.field", labelsfield)
    app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
    app1.ExecuteAndWriteOutput()
    
    return labeltifout

def unzipS1(zipfile, outdir):
    
    cmd=["unzip",zipfile,"-d", outdir]
    
    safefile = os.path.join(outdir,os.path.basename(zipfile)[:-4]+".SAFE")
    
    if os.path.exists(safefile) :
        return safefile
    
    process_command(cmd)
    
    if not os.path.exists(safefile) :
        print("Error: Cannot unzip "+zipfile)
        exit()
        
    return safefile
    
if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        RPG labels image reference, S1 Calibration, orhorectification, Sigma40, dem, slope, aspect and incidence angle processing
        """)
    
    parser.add_argument('-indir', action='store', required=True, help='Directory containing Sentinel1 .zip files')
    parser.add_argument('-tilename', action='store', required=True, help='MGRS tile name to process, generate RPG_[tilename].TIF from database')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    parser.add_argument('-polar', choices=['vv', 'vh'],  default='vh', required=False, help='[Optional] Process VV or VH polarization, default vh')
    parser.add_argument('-theta', choices=['loc', 'elli'],  default='loc', required=False, help='[Optional]Create local incidence raster or incidence raster from ellipsoid, default loc')
    parser.add_argument('--overwrite', dest='overwrite', action='store_true',required=False, help='[Optional] Overwrite already existing files (default False)')
    parser.set_defaults(overwrite=False)
    
    
    args=parser.parse_args()
    
    overwrite = args.overwrite
    
    if args.tilename.find("_") >= 0 :
        print("Error: '_' character is forbidden in -tilename string")
        exit()
    
    procstart = datetime.datetime.now()
    
    labelimgref = create_labelimage_ref(args.tilename, args.outdir)
    
    
    raster = gdal.Open(labelimgref)
    #DEBUG
    band = raster.GetRasterBand(1)
    try:
        band.ComputeStatistics(0)
        print("Statistics computed.")
    except Exception as inst:
        print(inst)
        exit()
        
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    if proj.IsGeographic() :
        print("Error: Reference image must be in projected coordinates in order to create slope")
        exit()
    
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
        
    demdir = os.path.join(args.outdir,"DEM")
    sig40dir = os.path.join(args.outdir,"Sigma40")
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
    if not os.path.exists(demdir):
        os.mkdir(demdir)
    if not os.path.exists(sig40dir):
        os.mkdir(sig40dir)
        
    indir=[]
    indir=search_files(args.indir, 'S1', 'zip', 'f')
    
    zonedemfile = create_zone_dem_aspect_and_slope(labelimgref,args.tilename,demdir)
    
    dejafait=[]
    for filez in indir:
        if filez in dejafait:
            continue
        
        tmpdir=os.path.join(args.indir,"tmp")
        if os.path.exists(tmpdir):
            shutil.rmtree(tmpdir)
        os.mkdir(tmpdir)
        if not os.path.exists(tmpdir):
            print("Error: Cannot create 'tmp' dir in "+args.indir)
            exit()

        bname = os.path.basename(filez).split(".")[0]
        splitbname = bname.split("_")
        plateforme = splitbname[0]
        acqdate_start = splitbname[4]  
        acqdate_end = splitbname[5]
        absorbit = splitbname[6]
        
        outfilebase = os.path.join(args.outdir,"_".join(splitbname[:3])+"_"+args.tilename.upper()+"_"+acqdate_start)
        outfile = outfilebase +"_"+args.polar.upper() +".TIF"
          
#         file = unzipS1(filez,tmpdir)

        setl = [indir.index(i) for i in indir if (acqdate_start.split('T')[0] in i) and (absorbit in i) and (plateforme in i)]
        
        sarfiles=[]
        annofiles=[]
        for ind in setl:
            if not os.path.exists(outfile) or overwrite : 
                xfile = unzipS1(indir[ind],tmpdir)
            else:
                print(outfile + " already exist.")
                print("Passing " + indir[ind])
                dejafait.append(indir[ind])
                continue
            
            in1vv = search_files(xfile, '-'+args.polar+'-', 'tiff', 'f')
            if len(in1vv) !=1 :
                print("Error: Can't find tiff files in "+xfile)
            else:
                sarfiles.append(in1vv[0])
                dejafait.append(indir[ind])
            
            anno1vv = search_files(xfile, '-'+args.polar+'-', 'xml', 'f')
            for a in anno1vv:
                if a.find("calibration") >= 0 or a.find("noise") >= 0:
                    continue
                annofiles.append(a)
        
        
        if len(annofiles) != len(sarfiles):
            print("Error on sarfiles or annofiles")
            print(annofiles)
            print(sarfiles)
            print("Skipping...")
            continue

        if not os.path.exists(outfile) or overwrite :        
            otberror = otb_s1_pipeline(sarfiles, labelimgref, outfile)
            if otberror:
                continue
        
        auxdir = os.path.join(os.path.dirname(outfile),"auxfiles")
        imgsp = os.path.basename(outfile).split("_")
         
        out_THETASURF = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETASURF.TIF")
        out_THETAELLI = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETAELLI.TIF")
        out_THETALOC = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETALOC.TIF")
        out_SIGMA40 = os.path.join(sig40dir,"_".join(imgsp[:-1])+"_SIGMA40.TIF")
 
        if not os.path.exists(auxdir):
            os.mkdir(auxdir)
         
        thvrtcsv=None
        if args.theta == "elli":
            if not os.path.exists(out_THETAELLI) or overwrite : 
                thvrtcsv = create_incidenceAngle_from_surface_raster(annofiles, outfile,out_THETASURF)
                create_incidenceAngle_from_ellipsoid_raster(outfile,zonedemfile,out_THETASURF,out_THETAELLI)
        elif args.theta == "loc":
            if not os.path.exists(out_THETALOC) or overwrite : 
                thvrtcsv = create_incidenceAngle_from_surface_raster(annofiles, outfile,out_THETASURF)
                thetaimg = create_local_incidenceAngle_raster(annofiles, outfile, zonedemfile, out_THETASURF,out_THETALOC)
        
            if not os.path.exists(out_SIGMA40) or overwrite :
                sigma0_at_40d(outfile, thetaimg, out_SIGMA40)
        
        print("Removing temp files...")
        if os.path.exists(tmpdir):
            shutil.rmtree(tmpdir)
        if thvrtcsv:
            for delfile in thvrtcsv:
                if os.path.exists(delfile):
                    os.remove(delfile)
        
        
    procend = datetime.datetime.now()
    proctime = procend -procstart
    print("Total elapse time : "+str(proctime))

    
        
        
    
