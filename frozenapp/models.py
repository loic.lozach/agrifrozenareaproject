from django.contrib.gis.db import models
from django.utils import timezone


class ParcelleGraphique(models.Model):
    id_parcel = models.CharField(max_length=10)
    surf_parc = models.FloatField()
    code_cultu = models.CharField(max_length=3)
    code_group = models.CharField(max_length=2)
    culture_d1 = models.CharField(max_length=3, null=True, blank=True)
    culture_d2 = models.CharField(max_length=3, null=True, blank=True)
 
    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField(srid=2154)
 
    # Returns the string representation of the model.
    def __str__(self):
        return self.id_parcel

class TileEnvelope(models.Model):
    fid = models.BigIntegerField()
    area = models.FloatField()
    perimeter = models.FloatField()
    tile = models.CharField(max_length=254)
    geom = models.MultiPolygonField(srid=2154)
    
    parcelles = models.ManyToManyField(ParcelleGraphique)
    
    def __str__(self):
        return self.tile

class Sentinel1Image(models.Model):
    tile = models.ForeignKey(TileEnvelope, on_delete=models.CASCADE)
    sentinel1_date = models.DateTimeField()
    sentinel1_pltf = models.CharField(max_length=3,null=True,blank=True)  
    sentinel1_orbit = models.CharField(max_length=3,null=True,blank=True)
    processed = models.BooleanField(default=False) 
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["sentinel1_date", 'tile'], 
                             name='datepartile_unique'
                             ),
            ]
        
    def __str__(self):
#         tz = timezone.utc
#         utcsd = tz.localize(self.sentinel1_date)
        return str(self.sentinel1_pltf +"_"+str(self.tile)+"_"+str(self.sentinel1_date)) #.isoformat(timespec='seconds')

    
class ParcelEvent(models.Model):
    parcelle = models.ForeignKey(ParcelleGraphique, on_delete=models.CASCADE)
    sentinel1 = models.ForeignKey(Sentinel1Image, on_delete=models.CASCADE)
    maxrefsigma40 = models.FloatField(null=True,blank=True)
    meansigma40 = models.FloatField(null=True,blank=True)
    meantemp = models.FloatField(null=True,blank=True)
    frozen_type = models.IntegerField(null=True,blank=True)
    parcelle_type = models.IntegerField()
    
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["parcelle", 'sentinel1'], 
                             name='parcelpardate_unique'
                             ),
            ]
        
    def __str__(self):
        return str(self.parcelle) +","+str(self.sentinel1)+","+str(self.meansigma40)+","+str(self.meantemp)
     


    

