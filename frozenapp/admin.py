from django.contrib.gis import admin
from .models import ParcelleGraphique, TileEnvelope

admin.site.register(ParcelleGraphique, admin.OSMGeoAdmin)

@admin.register(TileEnvelope)
class TileEnvelopeAdmin(admin.OSMGeoAdmin):
    def get_queryset(self, request):
        tileenvelope_qs = super().get_queryset(request)
        tileenvelope_pqs = tileenvelope_qs.prefetch_related('parcelles')
        return tileenvelope_pqs