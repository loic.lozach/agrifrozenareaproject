import os, glob, csv, datetime, zipfile
import xml.etree.ElementTree as ET
import logging
from django.contrib.gis.utils import LayerMapping
from frozenapp.models import *
from django.contrib.gis.db import models
from itertools import islice
import numpy as np

logger = logging.getLogger(__name__)

parcellegraphique_mapping = {
    'id_parcel' : 'ID_PARCEL',
    'surf_parc' : 'SURF_PARC',
    'code_cultu' : 'CODE_CULTU',
    'code_group' : 'CODE_GROUP',
    'culture_d1' : 'CULTURE_D1',
    'culture_d2' : 'CULTURE_D2',
    'mpoly' : 'MULTIPOLYGON',
}

tileenvelope_mapping = {
    'fid': 'FID',
    'area': 'AREA',
    'perimeter': 'PERIMETER',
    'tile': 'TILE',
    'geom': 'MULTIPOLYGON',
}


cult_classes = {
    '1':1,
    '2':1,
    '3':1,
    '4':1,
    '5':1,
    '6':1,
    '7':1,
    '8':1,
    '9':1,
    '14':1,
    '15':1,
    '16':1,
    '17':1,
    '18':1,
    '19':1,
    '20':2,
    '21':2,
    '22':2,
    '23':2,
    '25':1,
    '26':1,
        }

parcels_shp = '/work/python/data/RGP_BRETAGNE-BREST.shp'
tile_T30UVU = '/work/python/data/ENVELOPES/TILE_T30UVU.shp'
envelopes_dir = '/work/python/data/ENVELOPES'

def envelopes_import(envelopes_dir, verbose=True):
    nbparcelles = ParcelleGraphique.objects.count()
    if nbparcelles == 0 :
        print("No parcels polygons found! Use rgp_import first. Exiting")
        exit()
        
    for tile in glob.glob(os.path.join(envelopes_dir,'*.shp')):
        lm = LayerMapping(TileEnvelope, tile, tileenvelope_mapping, transform=False)
        lm.save(strict=True, verbose=verbose)
        
    it_tiles = TileEnvelope.objects.all().iterator()
    for tile in it_tiles:
        qs_interparcels = ParcelleGraphique.objects.filter(mpoly__intersects=tile.geom)
        if len(qs_interparcels) == 0:
            print("No parcels found for tile "+tile.tile)
            continue
        if verbose:
            print(len(qs_interparcels))
            print("%i parcels found for tile ", len(qs_interparcels))
            
        tile.parcelles.set(qs_interparcels)
            
        

def rpg_import(rgp_shp, verbose=True):
    nbparcelles = ParcelleGraphique.objects.count()
    if nbparcelles != 0 :
        print("RPG has already been initialized. Exiting.")
        exit()
    lm = LayerMapping(ParcelleGraphique, rgp_shp, parcellegraphique_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)


def sentinel1Image_import(tile_str, calibrated_S1_dir, ZIP_S1_dir, verbose=True):
    #get TileEnveloppe
    qs_tile = TileEnvelope.objects.filter(tile__exact=tile_str)
    if len(qs_tile) != 1:
        print(tile_str+" not found")
        exit()
    #list dir for cals1
    for tif in glob.glob(os.path.join(calibrated_S1_dir,'*.TIF')):
        #get datetime et pltf 
        tif_split = os.path.basename(tif[:-4]).split("_")
        tif_date = datetime.datetime.strptime(tif_split[4], "%Y%m%dT%H%M%S")
        tif_pltf = tif_split[0]
        tif_tile = tif_split[3]
        
        if tif_tile != tile_str:
            print("Wrong tile string in image file name. "+tif_tile+" instead of "+tile_str+".")
            exit()
        
        imgexist = Sentinel1Image.objects.filter(tile=qs_tile[0], 
                                      sentinel1_date= tif_date
                                      ).exists()
        if imgexist:
            print(os.path.basename(tif) + " already exists in database. Pass")
            continue
        #cherche metadata dans GRD_IW_S1_dir, get orbit
        grd_dir = glob.glob(os.path.join(ZIP_S1_dir,'*_'+tif_split[4]+'*.zip'))
        if len(grd_dir) == 0:
            print("Can't find "+os.path.join(ZIP_S1_dir,'*_'+tif_split[4]+'*.zip')+" file")
            exit()
        
        with zipfile.ZipFile(grd_dir[0]) as zf:
            anno_file = None
            for zc in zf.namelist():
                zcsplit = zc.split("/")
                if len(zcsplit) == 3 and zcsplit[1] == "annotation" and "-vh-" in zcsplit[2] and ".xml" in zcsplit[2]:
                    anno_file = zc
                    break
            
            if None == anno_file:
                print("Can't find "+os.path.join(os.path.join(grd_dir[0],"annotation"),'*-vh-*.xml')+" file")
                exit()
        
            anno_open = zf.open(anno_file)
            tree = ET.parse(anno_open)
            root = tree.getroot()
            orbtag = root.find(".//generalAnnotation/productInformation/pass").text
            if orbtag == "Descending":
                tif_orb = "DES"
            elif orbtag == "Ascending":
                tif_orb = "ASC"
            else:
                print("Error on orbit metadata, found : "+orbtag)
                exit()
        #create object
        obj,created = Sentinel1Image.objects.get_or_create(tile=qs_tile[0], 
                                            sentinel1_date= tif_date,
                                            sentinel1_pltf=tif_pltf,
                                            sentinel1_orbit=tif_orb
                                            )
        if verbose:
            if created :
                print(str(obj)+" has been created")
            else:
                print(str(obj)+" already in database")
                


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
    
def find_temp(tempdata, parcelid, date_str):
    #print("finding "+ parcelid + " on "+date_str)
    #print(str(len(tempdata)))
    indexdate=0
    it=0
    found=False
    for tp in tempdata[0]:
        if tp.find(date_str) >= 0:
            indexdate = it
            found=True
        it+=1
    
    if not found :
        print("Error: Can't find date in temperature csv file")
        exit()
    
    #print(str(indexdate))
    indexparc=0
    it=0
    found=False
    for tp in tempdata:        
        if it == 0:
            it+=1
            continue
        
        if int(tp[0]) == int(parcelid):
            return tp[indexdate]
        it+=1
        
    if not found :
        print("Error: Can't find parcel in temperature csv file")
        exit()
    
    

def parcels_meanvalues_import2(tile_str, sigma40_csv, temperature_csv, verbose=True):
    startproc = datetime.datetime.now()
    qs_tile = TileEnvelope.objects.filter(tile__exact=tile_str)
    
    if len(qs_tile) != 1:
        print(tile_str+" not found")
        exit()
    s_data=[]
    with open(sigma40_csv) as f:
        for line in f:
            sp_line = list(line.split(";"))
            rm=0
            for c in sp_line:
                if c == "\n":
                    sp_line.pop(rm)
                rm+=1
            s_data.append(sp_line)
    t_data=[]        
    with open(temperature_csv) as t:
        for line in t:
            sp_line = list(line.split(";"))
            rm=0
            for c in sp_line:
                if c == "\n":
                    sp_line.pop(rm)
                rm+=1
            t_data.append(sp_line)
    
    if len(s_data) != len(t_data):
        print("Sigma and temp count not equal. s_data = "+str(len(s_data))+" t_data = "+str(len(t_data)))
        exit()
    
    nbdates=-1
    dbimages=[]
    for col in s_data[0]:
#         print(str(col))
        nbdates+=1
        if nbdates == 0:
            continue  
        if col == '\n':
            print("bordel")
            exit()    
        s1_date = datetime.datetime.strptime(col, "%Y%m%dT%H%M%S")
        qs_s1date = Sentinel1Image.objects.filter(tile=qs_tile[0], 
                                                    sentinel1_date=s1_date
                                                    )
        if len(qs_s1date) != 1:
            print("Sentinel1 date "+col+" not found in database. Exiting")
            exit()
        if verbose:
            print(str(qs_s1date[0])+" found in database.")
        
        dbimages.append(qs_s1date[0])
    
    if len(s_data[0])-1 != len(dbimages):
        print("db request found different number of dates. exiting")
        exit()
    
    ls_data = len(s_data)
    i=-1
    opbar=0
#     doublonslog=[]
    objstocreate=[]
    nbc=0
    for sline in s_data:
        i+=1
        if i == 0:
#             print(str(sline))
            continue
        
        qs_pg = ParcelleGraphique.objects.filter(id_parcel__exact=sline[0])
        if len(qs_pg) != 1:
            print("Parcel with id "+sline[0]+" not found")
            continue
            
        if not qs_pg[0].code_group in cult_classes.keys():
            if verbose:
                print("Parcel with id "+sline[0]+" got wrong code_group="+qs_pg[0].code_group)
            continue
        parceltype = cult_classes[qs_pg[0].code_group]
        j=-1
        for scol in sline:
            j+=1
            if j == 0:
                continue
            
            if scol == '\n':
                print("bordel")
                exit()  
            
            
            if scol == "-nan" :
                v_meansigma40 = None
            elif isfloat(scol):                
                v_meansigma40 = float(scol)
                if v_meansigma40 <= 0:
                    continue
            else:
                continue
            
            v_meantemp = find_temp(t_data, sline[0], s_data[0][j])
            
            if v_meantemp == "-nan" :
                v_meantemp = None
            elif isfloat(v_meantemp):
                v_meantemp = float(v_meantemp)
            else:
                continue
            
            
            testexist = ParcelEvent.objects.filter(parcelle=qs_pg[0], sentinel1=dbimages[j-1])
            if len(testexist) > 0 :
                continue
            
            nbc+=1
            objstocreate.append(ParcelEvent(parcelle=qs_pg[0], sentinel1=dbimages[j-1], 
                                                             meansigma40 = v_meansigma40, 
                                                             meantemp = v_meantemp, 
                                                             parcelle_type = parceltype
                                                             ))
#             obj, created = ParcelEvent.objects.get_or_create(parcelle=qs_pg[0], sentinel1=dbimages[j-1], 
#                                                              meansigma40 = v_meansigma40, 
#                                                              meantemp = v_meantemp, 
#                                                              parcelle_type = parceltype
#                                                              )
            
#             if not created:
#                 doublonslog.append("DOUBLON "+str(obj))

            if verbose:
                print((str(objstocreate[nbc-1])+" cached"))
                
            pbar = int(i/ls_data*100)
            if pbar % 5 == 0 and pbar > opbar:
                opbar = pbar
                periodproc = datetime.datetime.now() - startproc
                print('Loading memory...')
                print(str(pbar)+" percent done ( "+str(i)+" / "+str(ls_data)+" )")
                print("Elapse time : "+str(periodproc))
                print("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
    
    startproc = datetime.datetime.now()
    print('Writing in database...')
    batch_size = 10000
    nbloop = nbc/ batch_size
    l=0
    while True:
        batch = list(islice(objstocreate, batch_size))
        if not batch:
            break
        ParcelEvent.objects.bulk_create(batch, batch_size)
        l+=1
        periodproc = datetime.datetime.now() - startproc
        print(str(l/nbloop)+" percent done ( "+str(l)+" / "+str(nbloop)+" )")
        print("Elapse time : "+str(periodproc))
        print("Remaining   : "+str(periodproc/((l/nbloop)/100)-periodproc))

#     with open(os.path.join(os.path.dirname(sigma40_csv),"found_doublons.log"),"x") as xlog:
#         for lg in doublonslog:
#             xlog.write(lg+"\n")
    
    periodproc = datetime.datetime.now() - startproc
    print("Total elapse time : "+str(periodproc))    

def parcels_meanvalues_import(tile_str, sigma40_csv, temperature_csv, verbose=True):
    startproc = datetime.datetime.now()
    qs_tile = TileEnvelope.objects.filter(tile__exact=tile_str)
    
    if len(qs_tile) != 1:
        print(tile_str+" not found")
        exit()
    
    print ("Reading CSVs....")
    s_data=[] 
    s_date=[]
    s_parc=[]
    with open(sigma40_csv) as f:
        i=0
        for line in f:
            sp_line = list(line.split(";"))
            try:
                rc = sp_line.index("\n") 
                sp_line.pop(rc)
            except:
                print("No return char in line")
                
            if i == 0:
                s_date = sp_line[1:]
            else:
                s_parc.append(sp_line[0])
                s_data.append(sp_line[1:])
            i+=1
            
            
    t_data=[]  
    t_date=[]
    t_parc=[]
    with open(temperature_csv) as t:
        i=0
        for line in t:
            sp_line = list(line.split(";"))
            try:
                rc = sp_line.index("\n") 
                sp_line.pop(rc)
            except:
                print("No return char in line")
                
            if i == 0:
                t_date = sp_line[1:]
            else:
                t_parc.append(sp_line[0])
                t_data.append(sp_line[1:])
            i+=1
    
    if len(s_data) != len(t_data):
        print("Sigma and temp data count not equal. s_data = "+str(len(s_data))+" t_data = "+str(len(t_data)))
        exit()
    if len(s_date) != len(t_date):
        print("Sigma and temp date count not equal. s_date = "+str(len(s_date))+" t_date = "+str(len(t_date)))
        exit()
    if len(s_parc) != len(t_parc):
        print("Sigma and temp parcels count not equal. s_parc = "+str(len(s_parc))+" t_parc = "+str(len(t_parc)))
        exit()
    
    print ("Getting sentinel date from db...")
    nbdates=0
    dbimages=[]
    for col in s_date: 
        s1_date = datetime.datetime.strptime(col, "%Y%m%dT%H%M%S")
        qs_s1date = Sentinel1Image.objects.filter(tile=qs_tile[0], 
                                                    sentinel1_date=s1_date
                                                    )
        if len(qs_s1date) != 1:
            print("Sentinel1 date "+col+" not found in database. Exiting")
            exit()
        if verbose:
            print(str(qs_s1date[0])+" found in database.")
        
        dbimages.append(qs_s1date[0])
        
        nbdates+=1
    
    if len(s_date) != len(dbimages):
        print("db request found different number of dates. exiting")
        exit()
    
    print('Loading db Objects in memory...')
    ls_data = len(s_parc)
    i=-1
    opbar=0
#     doublonslog=[]
    objstocreate=[]
    nbc=0
    for sp in s_parc:
        i+=1
        
        qs_pg = ParcelleGraphique.objects.filter(id_parcel__exact=sp)
        if len(qs_pg) != 1:
            print("Parcel with id "+sp+" not found")
            continue
            
        if not qs_pg[0].code_group in cult_classes.keys():
            if verbose:
                print("Parcel with id "+sp+" got wrong code_group="+qs_pg[0].code_group)
            continue
        parceltype = cult_classes[qs_pg[0].code_group]
        try:
            indp_temp = t_parc.index(sp)
        except:
            print("cant find Parcel with id "+sp+" for temperature")
            continue
        j=-1
        for od in dbimages:
            j+=1
            
            sigma = s_data[i][j]
            if sigma == "-nan" :
                v_meansigma40 = None
                continue
            elif isfloat(sigma):                
                v_meansigma40 = float(sigma)
                if v_meansigma40 <= 0:
                    continue
            else:
                continue
            
            v_meantemp = t_data[indp_temp][j]
            
            if v_meantemp == "-nan" :
                v_meantemp = None
                continue
            elif isfloat(v_meantemp):
                v_meantemp = float(v_meantemp)
            else:
                continue
            
            nbc+=1
            objstocreate.append(ParcelEvent(parcelle=qs_pg[0], sentinel1=od, 
                                                             meansigma40 = v_meansigma40, 
                                                             meantemp = v_meantemp, 
                                                             parcelle_type = parceltype
                                                             ))

            if verbose:
                print((str(objstocreate[nbc-1])+" cached"))
                
            pbar = int(i/ls_data*100)
            if pbar % 5 == 0 and pbar > opbar:
                opbar = pbar
                #Test
                fobj = objstocreate[0]
                lobj = objstocreate[-1]
                fqs_pe = ParcelEvent.objects.filter(parcelle=fobj.parcelle, sentinel1=fobj.sentinel1 )
                lqs_pe = ParcelEvent.objects.filter(parcelle=lobj.parcelle, sentinel1=lobj.sentinel1 )
                if len(fqs_pe) != 1 and len(lqs_pe) != 1 :     
                    print('Writing memory in database...')
                    ParcelEvent.objects.bulk_create(objstocreate)
                    objstocreate=[]
                    periodproc = datetime.datetime.now() - startproc
                    print(str(pbar)+" percent done ( "+str(i)+" / "+str(ls_data)+" )")
                    print("Elapse time : "+str(periodproc))
                    print("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
                    
                elif len(fqs_pe) == 1 and len(lqs_pe) == 1 :
                    objstocreate=[]
                    print("bulk already recorded. Passing...")
                    periodproc = datetime.datetime.now() - startproc
                    print(str(pbar)+" percent done ( "+str(i)+" / "+str(ls_data)+" )")
                    print("Elapse time : "+str(periodproc))
                    print("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
                
                else:
                    print("Error: bulk not fully recorded. Existing ")
                    return
    
    if len(objstocreate) == 0:
        print("TODO: fin de code inutile. a supprimer")
        print("Done.")
        return 
    
    fobj = objstocreate[0]
    lobj = objstocreate[-1]
    fqs_pe = ParcelEvent.objects.filter(parcelle=fobj.parcelle, sentinel1=fobj.sentinel1 )
    lqs_pe = ParcelEvent.objects.filter(parcelle=lobj.parcelle, sentinel1=lobj.sentinel1 )
    if len(fqs_pe) != 1 and len(lqs_pe) != 1 :
        print('Writing memory in database...')
        ParcelEvent.objects.bulk_create(objstocreate)
        objstocreate=[]
        periodproc = datetime.datetime.now() - startproc
        print("Elapse time : "+str(periodproc))
        print("Done.") 
        
    elif len(fqs_pe) == 1 and len(lqs_pe) == 1 :
        objstocreate=[]
        print("bulk already recorded. Done.")
        periodproc = datetime.datetime.now() - startproc
        print("Elapse time : "+str(periodproc))
        print("Done.") 
        
    else:
        print("Error: bulk not fully recorded. Existing ")
#     periodproc = datetime.datetime.now() - startproc
#     print("Loading memory elapse time : "+str(periodproc)) 
#     
#     startproc = datetime.datetime.now()
#     print('Writing in database...')
#     batch_size = 10000
#     nbloop = nbc/ batch_size
#     l=0
#     while True:
#         batch = list(islice(objstocreate, batch_size))
#         if not batch:
#             break
#         ParcelEvent.objects.bulk_create(batch, batch_size)
#         l+=1
#         periodproc = datetime.datetime.now() - startproc
#         print(str(l/nbloop)+" percent done ( "+str(l)+" / "+str(nbloop)+" )")
#         print("Elapse time : "+str(periodproc))
#         print("Remaining   : "+str(periodproc/((l/nbloop)/100)-periodproc))

#     with open(os.path.join(os.path.dirname(sigma40_csv),"found_doublons.log"),"x") as xlog:
#         for lg in doublonslog:
#             xlog.write(lg+"\n")
    
#     periodproc = datetime.datetime.now() - startproc
#     print("Total elapse time : "+str(periodproc))    
    
def test():
    tile_str='T32ULU'
    sigma40_csv='/data/ZoneA_T32ULU_rpg2019_nomerged/MEANSIGMA_T32ULU_20190901T054232_20200331T172303.csv'
    temperature_csv='/data/ZoneA_T32ULU_rpg2019_nomerged/MEANTEMP_T32ULU_20190901T054232_20200331T172303.csv'
    
    s_data=[] 
    s_date=[]
    s_parc=[]
    with open(sigma40_csv) as f:
        i=0
        for line in f:
            sp_line = list(line.split(";"))
            if i == 0:
                s_date = sp_line[1:]
            else:
                s_parc.append(sp_line[0])
                try:
                    rc = sp_line.index("\n") 
                    sp_line.pop(rc)
                    s_data.append(sp_line[1:])
                except:
                    s_data.append(sp_line[1:])
                    i+=1
                    continue
            i+=1
            
            
    t_data=[]  
    t_date=[]
    t_parc=[]
    with open(temperature_csv) as t:
        i=0
        for line in t:
            sp_line = list(line.split(";"))
            if i == 0:
                t_date = sp_line[1:]
            else:
                t_parc.append(sp_line[0])
                try:
                    rc = sp_line.index("\n") 
                    sp_line.pop(rc)
                    t_data.append(sp_line[1:])
                except:
                    t_data.append(sp_line[1:])
                    i+=1
                    continue
            i+=1
    
    print(s_parc[0]+" / "+t_parc[0])
    print(s_parc[1]+" / "+t_parc[1])
    print(s_parc[-1]+" / "+t_parc[-1])
    
#     sd_ind=[]
#     td_ind=[]
#     i=0
#     for sd in s_date:
#         try:
#             ind = t_date.index(sd)
#         except:
#             print("trouve pas "+sd+" !! bordel")
#             i+=1
#             continue
#         sd_ind.append(i)
#         td_ind.append(ind)
#         i+=1
#     nsd_ind = np.array(sd_ind)
#     ntd_ind = np.array(td_ind)
#     diff = nsd_ind - ntd_ind
#     result = np.where(diff != 0)
#     print(result)
#     
#     sp_ind=[]
#     tp_ind=[]
#     i=0
#     for sp in s_parc:
#         try:
#             ind = t_parc.index(sp)
#         except:
#             print("trouve pas "+sp+" !! bordel")
#             i+=1
#             continue
#         sp_ind.append(i)
#         tp_ind.append(ind)
#         i+=1
#     nsp_ind = np.array(sp_ind)
#     ntp_ind = np.array(tp_ind)
#     diff = nsp_ind - ntp_ind
#     result = np.where(diff != 0)
#     print(result)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
                             