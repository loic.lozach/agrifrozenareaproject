from django.apps import AppConfig


class FrozenappConfig(AppConfig):
    name = 'frozenapp'
