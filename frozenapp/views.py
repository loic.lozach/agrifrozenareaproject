from django.http import HttpResponse
from django.core.serializers import serialize
from .models import *
from django.views.generic import TemplateView


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def rpg_view(request):
    rpg_as_geojson = serialize('geojson', ParcelleGraphique.objects.all())
    return HttpResponse(rpg_as_geojson, content_type='json')



class MainPageView(TemplateView):
    template_name = 'index.html'
    
    