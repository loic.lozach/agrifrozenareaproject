from frozenapp.views import MainPageView
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]

# urlpatterns = [# rest of urls
#                url(r'^$', MainPageView.as_view()),]