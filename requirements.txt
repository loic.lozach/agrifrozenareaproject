# requirements.txt
# --------------------
# This file records the packages and requirements needed in order for
# sentinel DM to work as expected.


scipy
python-dateutil
html2text
npyscreen
requests
parsy
pyperclip
Django
psycopg2-binary
django-leaflet
django-extensions
eodag
#https://github.com/CS-SI/eodag-sentinelsat/archive/refs/heads/develop.zip
eodag-sentinelsat