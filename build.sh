# Look up how to launch docker if not running ``
echo USERUID=$UID > useruid.txt
#DATAPATH=/your/data/directory
docker-compose up -d --build
docker-compose exec web python manage.py makemigrations
docker-compose exec web python manage.py sqlmigrate frozenapp 0001 > frozenapp/migrations/frozenapp.sql
docker-compose exec web python manage.py migrate
