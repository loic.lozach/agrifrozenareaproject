FROM osgeo/gdal:ubuntu-full-3.0.4
SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

MAINTAINER Loic Lozach <loic.lozach[at]irstea[dot]fr>

USER root

RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
        sudo \
        ca-certificates \
        curl \
        make \
        cmake \
        g++ \
        gcc \
        git \
        libtool \
        swig \
        xvfb \
        wget \
        autoconf \
        automake \
        pkg-config \
        zip \
        zlib1g-dev \
        unzip \
 && rm -rf /var/lib/apt/lists/*
 
# ----------------------------------------------------------------------------
# OTB and TensorFlow dependencies
# ----------------------------------------------------------------------------
RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
 		libx11-dev \
 		libxext-dev \
 		libxt-dev \
 		libxi-dev \ 
 		libxrandr-dev \
 		libgl-dev \
 		libglu-dev \
 		libxinerama-dev \
 		libxcursor-dev \
        freeglut3-dev \
        libboost-date-time-dev \
        libboost-filesystem-dev \
        libboost-graph-dev \
        libboost-program-options-dev \
        libboost-system-dev \
        libboost-thread-dev \
        libcurl4-gnutls-dev \
        libexpat1-dev \
        libfftw3-dev \
        libgeotiff-dev \
        libglew-dev \
        libglfw3-dev \
        libgsl-dev \
        libinsighttoolkit4-dev \
        libkml-dev \
        libmuparser-dev \
        libmuparserx-dev \
        libopencv-core-dev \
        libopencv-ml-dev \
        libopenthreads-dev \
        libossim-dev \
        libpng-dev \
        libqt5opengl5-dev \
        libqwt-qt5-dev \
        libsvm-dev \
        libtinyxml-dev \
        qtbase5-dev \
        qttools5-dev \
        default-jdk \
        python3-pip \
        python3.6-dev \
        python3.6-gdal \
        python3-setuptools \
        libxmu-dev \
        qttools5-dev-tools \
        bison \
        software-properties-common \
        dirmngr \
        apt-transport-https \
        lsb-release \
 && rm -rf /var/lib/apt/lists/*

 
# ----------------------------------------------------------------------------
# Build OTB: Stage 1 (clone)
# ----------------------------------------------------------------------------
RUN mkdir -p /work/otb \
 && cd /work/otb \
 && git clone https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb.git otb \
 && cd otb \
 && git checkout release-7.1

# ----------------------------------------------------------------------------
# Build OTB: Stage 2 (superbuild)
# ----------------------------------------------------------------------------
#Patch cmake Wrong qt5 download url
#RUN sed -i 's,https://download.qt.io/archive/qt/5.10/5.10.1/single/qt-everywhere-src-5.10.1.tar.xz,https://download.qt.io/archive/qt/5.15/5.15.0/single/qt-everywhere-src-5.15.0.tar.xz,g' \
#	/work/otb/otb/SuperBuild/CMake/External_qt5.cmake \
#	&& sed -i 's,7e167b9617e7bd64012daaacb85477af,610a228dba6ef469d14d145b71ab3b88,g' \
#	/work/otb/otb/SuperBuild/CMake/External_qt5.cmake
RUN mkdir -p /work/otb/build \
 && cd /work/otb/build \
 && cmake /work/otb/otb/SuperBuild \
        -DUSE_SYSTEM_BOOST=ON \
        -DUSE_SYSTEM_CURL=ON \
        -DUSE_SYSTEM_EXPAT=ON \
        -DUSE_SYSTEM_FFTW=ON \
        -DUSE_SYSTEM_FREETYPE=ON \
        -DUSE_SYSTEM_GDAL=ON \
        -DUSE_SYSTEM_GEOS=ON \
        -DUSE_SYSTEM_GEOTIFF=ON \
        -DUSE_SYSTEM_GLEW=ON \
        -DUSE_SYSTEM_GLFW=ON \
        -DUSE_SYSTEM_GLUT=ON \
        -DUSE_SYSTEM_GSL=ON \
        -DUSE_SYSTEM_ITK=ON \
        -DUSE_SYSTEM_LIBKML=ON \
        -DUSE_SYSTEM_LIBSVM=ON \
        -DUSE_SYSTEM_MUPARSER=ON \
        -DUSE_SYSTEM_MUPARSERX=ON \
        -DUSE_SYSTEM_OPENCV=ON \
        -DUSE_SYSTEM_OPENTHREADS=ON \
        -DUSE_SYSTEM_OSSIM=ON \
        -DUSE_SYSTEM_PNG=ON \
        -DUSE_SYSTEM_QT5=ON \
        -DUSE_SYSTEM_QWT=ON \
        -DUSE_SYSTEM_TINYXML=ON \
        -DUSE_SYSTEM_ZLIB=ON \
        -DUSE_SYSTEM_SWIG=OFF \
        -DOTB_WRAP_PYTHON=OFF \
 && make -j $(grep -c ^processor /proc/cpuinfo)  
# && cat /work/otb/build/PROJ/src/PROJ-stamp/PROJ-build-error.log

# ----------------------------------------------------------------------------
# Build OTB AgriFrozenAreas
# ----------------------------------------------------------------------------


RUN cd /work/otb/otb/Modules/Remote \
 && git clone https://gitlab.irstea.fr/loic.lozach/agrifrozenareas.git \
 && cd /work/otb/build/OTB/build \
 && cmake /work/otb/otb \
	-DOTB_WRAP_PYTHON=ON \
	-DModule_AgriFrozenAreas=ON \
 && cd /work/otb/build/ \
 && make -j $(grep -c ^processor /proc/cpuinfo)


RUN apt-get update ; apt-get --assume-yes install apt-utils 
RUN apt-get --assume-yes install dialog
RUN apt-get --assume-yes install libncurses5-dev libncursesw5-dev
RUN apt-get --assume-yes install xclip
#RUN apt-get --assume-yes install gdal-bin libgdal-dev python3-gdal binutils libproj-dev

RUN mkdir /work/python
COPY requirements.txt /work/python
WORKDIR /work/python
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

COPY eodag_sentinelsat.py.patch /usr/local/lib/python3.6/dist-packages/eodag_sentinelsat/eodag_sentinelsat.py
# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------
RUN rm -rf /tmp/* /root/.cache && apt-get clean

# Replace 1000 with your user / group id
COPY useruid.txt /work/python
RUN source useruid.txt && \
    mkdir /data && \
    useradd -s /bin/bash -m --uid ${USERUID} otbuser && \
    echo "otbuser ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/otbuser && \
    chmod 0440 /etc/sudoers.d/otbuser && \
    chown ${USERUID}:${USERUID} -R /work && \
    chown ${USERUID}:${USERUID} -R /data

COPY . /work/python
RUN source useruid.txt && \
	chown ${USERUID}:${USERUID} -R /work/python
# ----------------------------------------------------------------------------
# Add important environment variables
# ----------------------------------------------------------------------------
ENV PATH="$PATH:/work/otb/superbuild_install/bin/:/work/python"
ENV PYTHONPATH="/work/otb/superbuild_install/lib/otb/python:/work/python/:$PYTHONPATH"
ENV OTB_APPLICATION_PATH="/work/otb/superbuild_install/lib/otb/applications"
ENV LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/work/otb/superbuild_install/lib/"
USER otbuser
ENV HOME=/home/otbuser
RUN alias ll='ls -al'
ENV TERM=xterm-256color
ENV GDAL_DATA=/usr/share/gdal/2.2/
ENV GDAL_DRIVER_PATH="disable"
ENV LC_NUMERIC=C
#RUN unset LD_LIBRARY_PATH
RUN alias ll='ls -al'
RUN sudo ln -s /usr/bin/python3 /usr/bin/python

#CDS ERA5-T API ACCES
RUN echo "url: https://cds.climate.copernicus.eu/api/v2" > $HOME/.cdsapirc
RUN echo "key: 49622:2ddcb71d-95fd-4a6e-a07f-384902df2eed" >> $HOME/.cdsapirc
RUN sudo pip3 install cdsapi


