import os, argparse, shlex, datetime
from frozenapp import load, process, eodags1



def init1_RPGImport(rpg_shp, verbose):
    load.rpg_import(rpg_shp, verbose)

def init2_TilesEnveloppesImport(envelopes_dir, verbose):
    load.envelopes_import(envelopes_dir, verbose)
    
def eodag_download( mrgstile, startdate, enddate, downdir):
    eds = eodags1.EodagS1()
    
    eds.search_and_download(0, mrgstile, startdate, enddate, downdir)
    
def eodag_restart( remaining_geojson, downdir, alternate_geojson=None):
    eds = eodags1.EodagS1()
    
    eds.import_geojson(remaining_geojson,alternate_products_geojson=alternate_geojson)
    
    eds.start_downloads(downdir)
    
def init3_Sentinel1ImagesImport(tile_str, calibrated_S1_dir, ZIP_S1_dir, verbose):
    load.sentinel1Image_import(tile_str, calibrated_S1_dir, ZIP_S1_dir, verbose)
    
def init4_ParcelsMeanvaluesImport(tile_str, sigma40_csv, temperature_csv, verbose):
    load.parcels_meanvalues_import(tile_str, sigma40_csv, temperature_csv, verbose)

def proc5_DetectFrozenArea(tile_str,fdetect_date,findmax,fconfig,temp_threshold):
    myfreeze = process.FreezeDetect()
    if None != fconfig:
        myfreeze.set_seuils(fconfig)
    if None != temp_threshold:
        myfreeze.set_lowtemp_threshold(temp_threshold)
    myfreeze.detect_frozenarea(tile_str,fdetect_date,findmax)
    
def proc6_finddetectables(tile_str,orbit,temp_threshold, wseason):
    myfreeze = process.FreezeDetect()
    if None != temp_threshold:
        myfreeze.set_lowtemp_threshold(temp_threshold)
    myfreeze.detectable_dates(tile_str, orbit, wseason)

def proc7_exportdate(tile_str,fdetect_date,outdir,suffix):
    myfreeze = process.FreezeDetect()
    myfreeze.export_shapefile(tile_str,fdetect_date,outdir,suffix)
    
def proc8_csvtimeserie(tile_str,parcel_id, outdir, suffix):
    myfreeze = process.FreezeDetect()
    myfreeze.export_parcel_csvtimeserie(tile_str,parcel_id, outdir, suffix)    
    
def proc9_batch_process_detectable(tile_str, orbit, findmax,fconfig,temp_threshold, wseason):
    myfreeze = process.FreezeDetect()
    if None != fconfig:
        myfreeze.set_seuils(fconfig)
    if None != temp_threshold:
        myfreeze.set_lowtemp_threshold(temp_threshold)
    myfreeze.batch_process_detectable(tile_str,orbit,findmax, wseason)
    
def proc10_batch_export_shapefile(tile_str, orbit, stats, outdir, suffix, wseason):
    myfreeze = process.FreezeDetect()
    myfreeze.batch_export_shapefile(tile_str, orbit, stats, outdir, suffix, wseason)
    
def proc11_normalize_rsquare(tile_str, year, outdir):
    myfreeze = process.FreezeDetect()
    myfreeze.normalize_rsquare(tile_str, year, outdir)
    
    
def run(*args):
    
    try:
        print(args)
        split_args = list(args)  #shlex.split(args[0])
        choix = int(args[0])
        split_args.pop(0)
        argsdict={}
        for arg in split_args:
            argsdict[arg.split("@")[0]]=arg.split("@")[1]
            if arg.split("@")[0] == 'fdetect_date':
                argsdict[arg.split("@")[0]] = datetime.datetime.strptime(arg.split("@")[1],"%Y%m%dT%H%M%S")
            if arg.split("@")[0] == 'v' or arg.split("@")[0] == 'stats':
                argsdict[arg.split("@")[0]]=bool(int(arg.split("@")[1]))
            if arg.split("@")[0] == 'year':
                argsdict[arg.split("@")[0]]=int(arg.split("@")[1])
            if arg.split("@")[0] == 'alternate_geojson':
                if arg.split("@")[1] == 'None':
                    argsdict[arg.split("@")[0]]=None
            if arg.split("@")[0] == 'outdir':
                if arg.split("@")[1] == 'None':
                    argsdict[arg.split("@")[0]]=None
            if arg.split("@")[0] == 'thdsigma':
                if arg.split("@")[1] == 'None':
                    argsdict[arg.split("@")[0]]=None
                else:
                    fconfig = {1:[],2:[],3:[]}
                    valspt = arg.split("@")[1].split(",")
                    for i in range(3):
                        ptspt = valspt[i].split(":")
                        try:
                            fconfig[i+1].append(float(ptspt[0]))
                            fconfig[i+1].append(float(ptspt[1]))
                        except ValueError:
                            print("thdsigma argument format is wrong. See help.")
                            exit()
                    argsdict[arg.split("@")[0]]=fconfig
            if arg.split("@")[0] == 'thdtemp':
                if arg.split("@")[1] == 'None':
                    argsdict[arg.split("@")[0]]=None
                else:
                    try:
                        argsdict[arg.split("@")[0]] = float(arg.split("@")[1])
                    except ValueError:
                        print("thdtemp argument is not a float. See help.")
                        exit()
            if arg.split("@")[0] == 'suffix':
                if arg.split("@")[1] == 'None':
                    argsdict[arg.split("@")[0]]=None
            if arg.split("@")[0] == 'wseason':
                if arg.split("@")[1] != 'all':
                    try:
                        wsyear = int(arg.split("@")[1])
                    except ValueError:
                        print("wseason argument format is wrong. See help.")
                        exit()
                    argsdict[arg.split("@")[0]]=wsyear
                
    except :
        choix = None
    
    verbose=False
    
    if choix == 1:
        init1_RPGImport(argsdict["rpg_shp"], argsdict["v"])
        print("done.")
        
    elif choix == 2:
        init2_TilesEnveloppesImport(argsdict["envelopes_dir"], argsdict["v"])
        print("done.")
        
    elif choix == 3:
        init3_Sentinel1ImagesImport(argsdict["tile_str"], argsdict["calibrated_S1_dir"], argsdict["ZIP_S1_dir"], argsdict["v"])
        print("done.")
        
    elif choix == 4:
        init4_ParcelsMeanvaluesImport(argsdict["tile_str"], argsdict["sigma40_csv"], argsdict["temperature_csv"], argsdict["v"])
        print("done.")
        
    elif choix == 5:                    
        proc5_DetectFrozenArea(argsdict["tile_str"],argsdict["fdetect_date"],argsdict["findmax"],argsdict["thdsigma"],argsdict["thdtemp"])
        print("done.")
    
    elif choix == 6:
        proc6_finddetectables(argsdict["tile_str"],argsdict["orbit"],argsdict["thdtemp"],argsdict["wseason"])
        print("done.")
        
    elif choix == 7:
        proc7_exportdate(argsdict["tile_str"],argsdict["fdetect_date"],argsdict["outdir"],argsdict["suffix"])
        
    elif choix == 8:
        proc8_csvtimeserie(argsdict["tile_str"],argsdict["parcelid"], argsdict["outdir"], argsdict["suffix"])
        
    elif choix == 9:   
        proc9_batch_process_detectable(argsdict["tile_str"],argsdict["orbit"], argsdict["findmax"],argsdict["thdsigma"],argsdict["thdtemp"],argsdict["wseason"])
        
    elif choix == 10:    
        proc10_batch_export_shapefile(argsdict["tile_str"],argsdict["orbit"], argsdict["stats"], argsdict["outdir"], argsdict["suffix"],argsdict["wseason"])
        
    elif choix == 11:    
        proc11_normalize_rsquare(argsdict["tile_str"],argsdict["year"], argsdict["outdir"])
        
    elif choix == 12:    
        eodag_download( argsdict["tile_str"],argsdict["startdate"], argsdict["enddate"], argsdict["downdir"])
        
    elif choix == 13:    
        eodag_restart( argsdict["remaining_geojson"], argsdict["downdir"], argsdict["alternate_geojson"])
        
    else:
        print("Wrong argument.")
        exit()

