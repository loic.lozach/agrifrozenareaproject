#!/usr/bin/python3
import io, os, sys, argparse, shlex, datetime, time
from subprocess import Popen, PIPE, STDOUT
import otbApplication

sq = r"""shp << '/data/S2Tiles/T30UVU.shp';
aoi in shp;
bp from NOW to NOW-3d;
pfname = s1;
ptype = GRD;
"""

def stream_process(process):
    go = process.poll() is None
    for line in process.stdout:
        print(line.strip())
    return go

def process_command(cmd):
#     my_env = os.environ.copy()
#     my_env["PATH"] = "/work/python/theia_download:" + my_env["PATH"]    , env=my_env
    print("Starting : "+" ".join(cmd))

    process = Popen(" ".join(cmd), stdout=PIPE, stderr=STDOUT, shell=True)
    while stream_process(process):
        time.sleep(0.1)
#     for line in process.stdout:
#         sys.stdout.buffer.write(line)
#         sys.stdout.buffer.flush()
#         # do stuff with the line variable here
#     process.wait()
#    p.wait()
#     output = p.communicate()[0]
#     if p.returncode != 0: 
#         print("process failed %d : %s" % (p.returncode, output))
#         exit()
#     print(output)
#     return p.returncode

#     while True:
#         output = process.stdout.readline()
#         if process.poll() != None or output.find("done.") >=0 :
#             break
#         if output :
#             print(output.strip())
#     rc = process.poll()  #communicate()[0]
#     return rc

    
def search_files(directory='.', resolution='S1', extension='SAFE', fictype='d'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit()
            
    return images

def gen_csv_otb(datelist, sarfiles,erafiles, ref, outsigmacsv, outtempcsv, eratypes):
    
    print("processing.......................................................")
    
    afa = otbApplication.Registry.CreateApplication("AgriFrozenAreas")
    s1apps=[]
    eraapps=[]
    manapps=[]
    bmxapps=[]
    i=0
    for sarfile in sarfiles:
        
        s1apps.append(otbApplication.Registry.CreateApplication("Superimpose"))
        s1apps[i].SetParameterString("inm",sarfile)
        s1apps[i].SetParameterString("out", str(i)+"temp.tif")
        s1apps[i].SetParameterString("interpolator","nn")
        s1apps[i].SetParameterString("inr",ref)
        s1apps[i].Execute()
        
#       otbcli_ManageNoData -in /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644.grib 
#        -out /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644_no.tif 
#        -mode changevalue -mode.changevalue.newv  283 
        manapps.append(otbApplication.Registry.CreateApplication("ManageNoData"))
        manapps[i].SetParameterString("in",erafiles[i])
        manapps[i].SetParameterString("out", str(i+100)+"temp.tif")
        manapps[i].SetParameterString("mode","changevalue")
        manapps[i].SetParameterFloat("mode.changevalue.newv",283)
        manapps[i].Execute()
        
#       otbcli_BandMathX -il /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644.grib 
#        -out /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644_bmx.tif 
#        -exp "(im1b1 == 283 ? im1b1 = mean(im1b1N5x5):im1b1)"
        bmxapps.append(otbApplication.Registry.CreateApplication("BandMathX"))
        bmxapps[i].AddImageToParameterInputImageList("il",manapps[i].GetParameterOutputImage("out"))
        bmxapps[i].SetParameterString("out", str(i+200)+"temp.tif")
        if eratypes[i] == "2m":
            bmxapps[i].SetParameterString("exp","(im1b1 == 283 ? im1b1 = mean(im1b1N5x5):im1b1)")
        elif eratypes[i] == "1000hpa":
            bmxapps[i].SetParameterString("exp","(im1b1 + 273.15)")
        else:
            print("ERA5 files type not recognize. exiting")
            exit()
            
        bmxapps[i].Execute()
        
        eraapps.append(otbApplication.Registry.CreateApplication("Superimpose"))
        eraapps[i].SetParameterInputImage("inm",bmxapps[i].GetParameterOutputImage("out"))
        eraapps[i].SetParameterString("out", str(i+300)+"temp.tif")
        eraapps[i].SetParameterString("interpolator","bco")
        eraapps[i].SetParameterString("inr",ref)
        eraapps[i].Execute()
        
        afa.AddImageToParameterInputImageList("ilsigma",s1apps[i].GetParameterOutputImage("out"))
        afa.AddImageToParameterInputImageList("iltemp",eraapps[i].GetParameterOutputImage("out"))
        i+=1
    
    afa.SetParameterStringList("datelist",datelist)
    afa.SetParameterString("inlabels",ref)
    afa.SetParameterString("out.sigma",outsigmacsv) 
    afa.SetParameterString("out.temp",outtempcsv) 
    afa.ExecuteAndWriteOutput()
    
    
        

def gen_csv(rpglabels_img, calibrated_S1_dir, resampled_ERA5_dir,
            out_dir, verbose=False):
    
    if not os.path.exists(calibrated_S1_dir):
        print("Error: Directory does not exist "+calibrated_S1_dir)
    if not os.path.exists(resampled_ERA5_dir):
        print("Error: Directory does not exist "+resampled_ERA5_dir)
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
        
        
    s1calfiles = search_files(calibrated_S1_dir,resolution='S1', extension='TIF', fictype='f')
    erafiles = search_files(resampled_ERA5_dir,resolution='ERA', extension='grib', fictype='f')
    if len(erafiles) != len(s1calfiles):
        print("Error: number of sentinel1 files and era5 files doesn't match")
        print("Exiting.")
        exit()
    
    tile_str=None
    s1strdates=[]
    s1datetimes=[]
    erafilesord=[]
    eratypes=[]
    i=0
    for s1 in s1calfiles:
        strdate=os.path.basename(s1)[:-4].split("_")[4]
        s1strdates.append(strdate)
        s1datetimes.append(datetime.datetime.strptime(strdate, "%Y%m%dT%H%M%S"))
        erafound=False
        for era in erafiles:
            if strdate in era:
                erafilesord.append(era)
                if "-2m" in era:
                    eratypes.append("2m")
                elif "-1000hpa" in era:
                    eratypes.append("1000hpa")
                else:
                    print("ERA5 files type not recognize for "+era+". exiting")
                    exit()
                erafound=True
                break
        if not erafound:
            print("Error: Can't find matching date for era file "+strdate)
            print("Exiting.")
            exit()
        tile_name = os.path.basename(s1)[:-4].split("_")[3]
        if tile_str == None :
            tile_str = tile_name
        else:
            if tile_str != tile_name:
                print("Error: wrong tile_str in image filename: "+s1)
                print("Exiting.")
                exit()
        i+=1
    
    mindate = min(s1datetimes)
    maxdate = max(s1datetimes)
    out_sigma40_csv = os.path.join(out_dir,"MEANSIGMA_"+tile_str+"_"+mindate.strftime("%Y%m%dT%H%M%S")
                                                                +"_"+maxdate.strftime("%Y%m%dT%H%M%S")
                                                                +".csv")
    out_temperature_csv = os.path.join(out_dir,"MEANTEMP_"+tile_str+"_"+mindate.strftime("%Y%m%dT%H%M%S")
                                                                +"_"+maxdate.strftime("%Y%m%dT%H%M%S")
                                                                +".csv")
    
    gen_csv_otb(s1strdates,s1calfiles,erafilesord, rpglabels_img, out_sigma40_csv, out_temperature_csv, eratypes)
    print("Done.")
    
if __name__ == "__main__":
    
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Freeze detection services
        """)

    subparsers = parser.add_subparsers(help='Choose service', dest="pipeline")

    # Short pipeline
    list_parser = subparsers.add_parser('init1', help="Import RPG shapefile into database, ONLY ONCE after build.")
    list_parser.add_argument('-rpg_shp', action='store', required=True, help="RPG shapefile")
    list_parser.add_argument('-v', action='store_true', help="[Optional] Verbose on")
    
    list_parser = subparsers.add_parser('init2', help="Import S2 tiles enveloppes into database, ONLY ONCE after build.")
    list_parser.add_argument('-envelopes_dir', action='store', required=True, help="Directory containing all S2 tiles enveloppes shapefiles")
    list_parser.add_argument('-v', action='store_true', help="[Optional] Verbose on")

    list_parser = subparsers.add_parser('loadS1', help="Import S1 images metadata into database.")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-calibrated_S1_dir', action='store', required=True, help="S1 Calibrated directory")
    list_parser.add_argument('-ZIP_S1_dir', action='store', required=True, help="S1 zipfile directory")
    list_parser.add_argument('-v', action='store_true', help="[Optional] Verbose on")
    
    list_parser = subparsers.add_parser('genCSV', help="Generate parcels mean value (Sigma, temperature) CSV files.")
    list_parser.add_argument('-rpglabels_img', action='store', required=True, help="Rasterized RPG labels image file")
    list_parser.add_argument('-calibrated_S1_dir', action='store', required=True, help="S1 Calibrated directory")
    list_parser.add_argument('-resampled_ERA5_dir', action='store', required=True, help="Superimposed temperature ERA5 images directory")
    list_parser.add_argument('-out_dir', action='store', required=True, help="Mean values output csv directory")
    list_parser.add_argument('-v', action='store_true', help="[Optional] Verbose on")
    
    list_parser = subparsers.add_parser('loadCSV', help="Import Parcels mean values into database.")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-sigma40_csv', action='store', required=True, help="Mean sigma40 generated csv file")
    list_parser.add_argument('-temperature_csv', action='store', required=True, help="Mean temperature generated csv file")
    list_parser.add_argument('-v', action='store_true', help="[Optional] Verbose on")
    
    list_parser = subparsers.add_parser('fdetect', help="Process Freeze Detection")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-fdetect_date', action='store', required=True, help="Sentinel1 datetime to process, ex. 20190131T175959")
    list_parser.add_argument('-findmax', choices=['closest', 'closest2max', 'range', 'p90'], required=False, default="closest", help="[Optional] Find sigma max algorithm, default 'closest'")
    list_parser.add_argument('-thdsigma', action='store', required=False, help="[Optional] Set freeze sigma thresholds for all parcel types (pt1, pt2, pt3) as 'pt1_valinf:pt1_valsup,pt2_valinf:pt2_valsup,pt3_valinf:pt3_valsup', default '3.48:5.25,2.81:3.53,2.0:2.86'")
    list_parser.add_argument('-thdtemp', action='store', required=False, help="[Optional] Set temperature threshold in kelvin, default 276.15 degK = 3 degC ")
    
    list_parser = subparsers.add_parser('finddates', help="Find detectable dates")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-orbit', choices=['ASC', 'DES'], required=False, default="DES", help="[Optional] Satellite orbit direction, default 'DES'")
    list_parser.add_argument('-thdtemp', action='store', required=False, help="[Optional] Set temperature threshold in kelvin, default 276.15 degK = 3 degC ")
    list_parser.add_argument('-wseason', action='store', required=False, default='all', help="[Optional] Year of winter season to process, ex: 2021 = from 09/2020 to 05/2021, default all")
    
    list_parser = subparsers.add_parser('normalize', help="Normalize detection on march-april months")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-year', action='store', required=True, help="Year of march-april months to normalize")
    list_parser.add_argument('-outdir', action='store', required=False, help="[Optional] Output directory for csv regession results")
    
    list_parser = subparsers.add_parser('exportdate', help="Export detected date to shapefile")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-fdetect_date', action='store', required=True, help="Sentinel1 datetime to process, ex. 20190131T175959")
    list_parser.add_argument('-outdir', action='store', required=True, help="Output directory for shapefile, create file FREEZEDETECT_[tile]_[date]_[suffix].shp")
    list_parser.add_argument('-suffix', action='store', required=False, help="[Optional] Add a suffix to shapefile name")
    
    list_parser = subparsers.add_parser('exportTScsv', help="Export parcel id time series in csv file")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-parcelid', action='store', required=True, help="PARCEL_ID from Reseau Parcellaire Graphique")
    list_parser.add_argument('-outdir', action='store', required=True, help="Output directory for shapefile, create file TIMESERIE_[tile]_[parcelid]_[suffix].csv")
    list_parser.add_argument('-suffix', action='store', required=False, help="[Optional] Add a suffix to csv name")
    
    list_parser = subparsers.add_parser('batchfdetect', help="Find detectable dates and process all Freeze Detection")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-orbit', choices=['ASC', 'DES'], required=False, default="DES", help="[Optional] Satellite orbit direction, default 'DES'")
    list_parser.add_argument('-findmax', choices=['closest', 'closest2max', 'range', 'p90'], required=False, default="closest", help="[Optional] Find sigma max algorythm, default 'closest'")
    list_parser.add_argument('-thdsigma', action='store', required=False, help="[Optional] Set freeze sigma thresholds for all parcel types (pt1, pt2, pt3) as 'pt1_valinf:pt1_valsup,pt2_valinf:pt2_valsup,pt3_valinf:pt3_valsup', default '3.48:5.25,2.81:3.53,2.0:2.86'")
    list_parser.add_argument('-thdtemp', action='store', required=False, help="[Optional] Set temperature threshold in kelvin, default 276.15 degK = 3 degC ")
    list_parser.add_argument('-wseason', action='store', required=False, default='all', help="[Optional] Year of winter season to process, ex: 2021 = from 09/2020 to 05/2021, default all")
    
    list_parser = subparsers.add_parser('batchexportdates', help="Export detected date to shapefile")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-orbit', choices=['ASC', 'DES'], required=False, default="DES", help="[Optional] Satellite orbit direction, default 'DES'")
    list_parser.add_argument('-stats', action='store_true', help="[Optional] Export ONLY freeze detection statistics by date into FREEZESTATS_[tile]_[suffix].csv")
    list_parser.add_argument('-outdir', action='store', required=True, help="Output directory for shapefile, create file FREEZEDETECT_[tile]_[date]_[suffix].shp")
    list_parser.add_argument('-suffix', action='store', required=False, help="[Optional] Add a suffix to shapefile name")
    list_parser.add_argument('-wseason', action='store', required=False, default='all', help="[Optional] Year of winter season to process, ex: 2021 = from 09/2020 to 05/2021, default all")
    
    list_parser = subparsers.add_parser('batchshp2zip', help="batch export shapefile to zipfile for Thisme ingestion")
    list_parser.add_argument('-indir', action='store', required=True, help="Directory to find recursively shapefiles")
    list_parser.add_argument('-outdir', action='store', required=True, help="Output directory for zipfiles")
    
    
    args=parser.parse_args()
    
    if args.pipeline == 'init1' :
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "1", "rpg_shp@"+args.rpg_shp, "v@"+str(int(args.v))
            ]
        process_command(cmd)
    
    elif args.pipeline == 'init2' :
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "2", "envelopes_dir@"+args.envelopes_dir, "v@"+str(int(args.v))
            ]
        process_command(cmd) 
        
    elif args.pipeline == 'loadS1' :
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "3", "tile_str@"+args.tile_str, "calibrated_S1_dir@"+args.calibrated_S1_dir, "ZIP_S1_dir@"+args.ZIP_S1_dir, "v@"+str(int(args.v))
            ]
        process_command(cmd) 
                
    elif args.pipeline == 'genCSV' :
        
        gen_csv( args.rpglabels_img, args.calibrated_S1_dir, args.resampled_ERA5_dir,
              args.out_dir, args.v
            )
    
    elif args.pipeline == 'loadCSV' :
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "4", "tile_str@"+args.tile_str, "sigma40_csv@"+args.sigma40_csv, "temperature_csv@"+args.temperature_csv, "v@"+str(int(args.v))
            ]
        process_command(cmd)
                   
    elif args.pipeline == 'fdetect' :
                
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "5", "tile_str@"+args.tile_str, "fdetect_date@"+args.fdetect_date, "findmax@"+args.findmax, "thdsigma@"+str(args.thdsigma) , "thdtemp@"+str(args.thdtemp)
            ]
        process_command(cmd)
        
    elif args.pipeline == 'finddates' :
        
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "6", "tile_str@"+args.tile_str, "orbit@"+args.orbit, "thdtemp@"+str(args.thdtemp), "wseason@"+str(args.wseason)
            ]
        process_command(cmd)
        
    elif args.pipeline == 'normalize' :
        
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "11", "tile_str@"+args.tile_str, "year@"+args.year, "outdir@"+str(args.outdir)
            ]
        process_command(cmd)
        
    elif args.pipeline == 'exportdate' :
        
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "7", "tile_str@"+args.tile_str, "fdetect_date@"+args.fdetect_date, "outdir@"+args.outdir, "suffix@"+str(args.suffix)
            ]
        process_command(cmd)
    
    elif args.pipeline == 'exportTScsv' :
        
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "8", "tile_str@"+args.tile_str, "parcelid@"+args.parcelid, "outdir@"+args.outdir, "suffix@"+str(args.suffix)
            ]
        process_command(cmd) 
        
    elif args.pipeline == 'batchfdetect' :
                
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "9", "tile_str@"+args.tile_str, "orbit@"+args.orbit, "findmax@"+args.findmax, "thdsigma@"+str(args.thdsigma) , "thdtemp@"+str(args.thdtemp), 
              "wseason@"+str(args.wseason)
            ]
        process_command(cmd)
        
    elif args.pipeline == 'batchexportdates' :
        
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "10", "tile_str@"+args.tile_str, "orbit@"+args.orbit, "stats@"+str(int(args.stats)), "outdir@"+args.outdir, "suffix@"+str(args.suffix), 
              "wseason@"+str(args.wseason)
            ]
        process_command(cmd) 
    
    elif args.pipeline == 'batchshp2zip' :
        
        if not os.path.exists(args.indir):
            print("Error: Directory does not exist "+resampled_ERA5_dir)
        if not os.path.exists(args.outdir):
            os.mkdir(out_dir)
            
            
        shpfiles = search_files(args.indir,resolution='FREEZEDETECT_', extension='shp', fictype='f')
        
        for shp in shpfiles :
            outzip = os.path.basename(shp)[:-3]+"zip"
            ashp = shp[:-3]+"*"
        
            cmd=["zip", os.path.join(args.outdir,outzip) , ashp ]
            process_command(cmd) 
    
        
        
        
        