#!/usr/bin/python3
import os, sys, argparse, shlex, glob, time
import cdsapi
from subprocess import Popen, PIPE, STDOUT
from osgeo import gdal, osr, ogr


def stream_process(process):
    go = process.poll() is None
    for line in process.stdout:
        print(line.strip())
    return go

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    process = Popen(" ".join(cmd), stdout=PIPE, stderr=STDOUT, shell=True)
    while stream_process(process):
        time.sleep(0.1)

def download_cds(m_year, m_month, m_day, m_time, m_latmax, m_lonmin, m_latmin, m_lonmax, outfile):
    c = cdsapi.Client()
    requ = (
    'reanalysis-era5-land',
    {
        'format': 'grib',
        'variable': '2m_temperature',
        'year': m_year,#AAAA
        'month': m_month,#MM
        'day': m_day,#DD
        'time': m_time,#HH:00
        'area': [
            m_latmax, m_lonmin, m_latmin,
            m_lonmax,
        ],
    },
    outfile)
    try:
        c.retrieve(requ[0],requ[1],requ[2])
    except Exception as err:
        print(err)
        return requ
    
    return requ
    
def download_cds_inst(m_year, m_month, m_day, m_time, m_latmax, m_lonmin, m_latmin, m_lonmax, outfile):
    c = cdsapi.Client()
    requ = (
    'reanalysis-era5-pressure-levels',
    {
        'product_type': 'reanalysis',
        'format': 'grib',
        'variable': 'temperature',
        'pressure_level': '1000',
        'year': m_year,#AAAA
        'month': m_month,#MM
        'day': m_day,#DD
        'time': m_time,#HH:00
        'area': [
            m_latmax, m_lonmin, m_latmin,
            m_lonmax,
        ],
    },
    outfile)
    try:
        c.retrieve(requ[0],requ[1],requ[2])
    except Exception as err:
        print(err)
        requ[0]=err
        return requ
    
    return requ

def search_files(directory='.', resolution='S1', extension='SAFE', fictype='d'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit()
            
    return images

def round_s1time(s1time):
    m_hour = int(s1time[:2])
    m_minu = int(s1time[2:4])
    m_seco = int(s1time[4:6])
    
    if m_seco >= 30 :
        m_minu += 1;
    if m_minu >= 30 :
        m_hour += 1;
    if m_hour == 24:
        m_hour -= 1;
    st_hour = str(m_hour)
    if m_hour < 10 :
        st_hour = "0"+st_hour
    return st_hour+":00"

def round_extend(points, supinf):
    
    for pt in points:
        if supinf == "sup":
            pt.SetPoint(0,int(pt.GetX()*10)/10+0.1,int(pt.GetY()*10)/10+0.1)
        elif supinf == "inf":
            pt.SetPoint(0,int(pt.GetX()*10)/10-0.1,int(pt.GetY()*10)/10-0.1)
        
#     val=val*10
#     pe = int(val)
#     deci = val/pe
#     if supinf == "sup":
#         if deci < 0.25 :
#             deci = 0.25
#         elif 0.25 <= deci < 0.5:
#             deci = 0.5
#         elif 0.5 <= deci < 0.75:
#             deci = 0.75
#         else:
#             deci = 0.
#             pe += 1
#     elif supinf == "inf":
#         if deci < 0.25 :
#             deci = 0.
#         elif 0.25 <= deci < 0.5:
#             deci = 0.25
#         elif 0.5 <= deci < 0.75:
#             deci = 0.5
#         else:
#             deci = 0.75
#     
#     return (pe+deci)/10
    
def get_ref_extend(imgref):
    raster = gdal.Open(imgref)
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    #print("ref: "+raster.GetProjection())
    upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
    cols = raster.RasterXSize
    rows = raster.RasterYSize
     
    ulx = upx + 0*xres + 0*xskew
    uly = upy + 0*yskew + 0*yres
     
    llx = upx + 0*xres + rows*xskew
    lly = upy + 0*yskew + rows*yres
     
    lrx = upx + cols*xres + rows*xskew
    lry = upy + cols*yskew + rows*yres
     
    urx = upx + cols*xres + 0*xskew
    ury = upy + cols*yskew + 0*yres
    
    pointUL, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointUL.AddPoint(ulx, uly)
    pointLR.AddPoint(lrx, lry)
    pointLL.AddPoint(llx, lly)
    pointUR.AddPoint(urx, ury)
    
    spatialRef = osr.SpatialReference()
    spatialRef.ImportFromEPSG(4326) 

    if not proj.IsSame(spatialRef) :
        coordTransform = osr.CoordinateTransformation(proj,spatialRef)
        pointUL.Transform(coordTransform)
        pointLR.Transform(coordTransform)
        pointLL.Transform(coordTransform)
        pointUR.Transform(coordTransform)
    
    print(str(pointUR))
    print(str(pointUL))
    print(str(pointLL))
    print(str(pointLR))
    
    latmax = str(max([pointUL.GetY(),pointLL.GetY(),pointUR.GetY(),pointLR.GetY()])+0.2)
    latmin = str(min([pointUL.GetY(),pointLL.GetY(),pointUR.GetY(),pointLR.GetY()])-0.2)
    lonmax = str(max([pointUL.GetX(),pointLL.GetX(),pointUR.GetX(),pointLR.GetX()])+0.2)
    lonmin = str(min([pointUL.GetX(),pointLL.GetX(),pointUR.GetX(),pointLR.GetX()])-0.2)
    
#     pointLL.SetPoint(0,round_extend(pointLL.GetX(), "inf"),round_extend(pointLL.GetY(), "inf"))
#     pointUR.SetPoint(0,round_extend(pointUR.GetX(), "sup"),round_extend(pointUR.GetY(), "sup"))
    print([latmax, lonmin, latmin, lonmax])
    
    return [latmax, lonmin, latmin, lonmax]

if __name__ == "__main__":
    
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Download services
        """)

    subparsers = parser.add_subparsers(help='Choose server', dest="pipeline")

    # Short pipeline
    list_parser = subparsers.add_parser('eodag', help="Download S1 through Eodag API")
    list_parser.add_argument('-downdir', action='store', required=True, help="Download directory")
    list_parser.add_argument('-tile_str', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-startdate', action='store', required=True, help="Starting date S1, ex. 2020-08-31")
    list_parser.add_argument('-enddate', action='store', required=True, help="Ending date S1, ex. 2020-09-31")
    
    list_parser = subparsers.add_parser('eodagrestart', help="Restart download S1 through Eodag API using Eodag SearchResult geojson files")
    list_parser.add_argument('-downdir', action='store', required=True, help="Download directory")
    list_parser.add_argument('-remaining_geojson', action='store', required=True, help="Remaining geojson file created from eodag pipeline")
    list_parser.add_argument('-alternate_geojson', action='store', required=False, help="[Optional] Alternate geojson file created from eodag pipeline")
    
    list_parser = subparsers.add_parser('cds', help="Download ERA5 temperature image file according to S1 dates")
    list_parser.add_argument('-s1dir', action='store', required=True, help="Calibrated Sentinel1 directory")
    list_parser.add_argument('-ref', action='store', required=True, help="AOI image reference")
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-gap', choices=['3m', '6d'], required=False, default='land', help="[Optional] Query ERA5-Land reanalysis collection with 3 month gap (default '3m') OR ERA5 basic reanalysis collection with 6 days gap ('6d') ")
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')
        
    args=parser.parse_args()
    
    if args.pipeline == 'eodag' :
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "12", "downdir@"+args.downdir, "tile_str@"+args.tile_str, "startdate@"+args.startdate, "enddate@"+args.enddate
            ]
        process_command(cmd)
    elif args.pipeline == 'eodagrestart' :
        cmd=["python", "manage.py", "runscript", "freezeDetectWrapper", "--traceback", "--script-args",
              "13", "remaining_geojson@"+args.remaining_geojson, "downdir@"+args.downdir, "alternate_geojson@"+str(args.alternate_geojson)
            ]
        process_command(cmd)
        
    elif args.pipeline == 'cds' :
        outabs = os.path.abspath(args.outdir)
        if not os.path.exists(outabs):
            os.makedirs(outabs)
        
#         s1files = search_files(args.s1dir, 'S1', 'tif', fictype='f')
        s1files = glob.glob(os.path.join(args.s1dir, 'S1*.TIF'))
        extend = get_ref_extend(args.ref)
        errfile = os.path.join(args.outdir, "download_error.log")
        with open(errfile, 'w') as err:
            for s1 in s1files:
                stdatetime = os.path.basename(s1)[:-4].split("_")[4]
                stdate = stdatetime.split("T")[0]
                sttime = stdatetime.split("T")[1]
                rdtime = round_s1time(sttime) 
                
                if args.gap == '3m':
                    outfile = os.path.join(args.outdir,"ERA5-2m_"+args.zone+"_"+stdatetime+".grib")
                    print("Downloading CDS : "+outfile)
                    request = download_cds(stdate[:4], stdate[4:6], stdate[6:8], rdtime, extend[0], extend[1], extend[2], extend[3], outfile)
                else:
                    outfile = os.path.join(args.outdir,"ERA5-1000hpa_"+args.zone+"_"+stdatetime+".grib")
                    print("Downloading CDS : "+outfile)
                    request = download_cds_inst(stdate[:4], stdate[4:6], stdate[6:8], rdtime, extend[0], extend[1], extend[2], extend[3], outfile)
                
                if not os.path.exists(outfile):
                    err.write(request[2]+";"+request[1]+"\n")
                    err.write(request[0]+"\n")
        

        
        
        
        
        
        